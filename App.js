/* eslint-disable react/no-unstable-nested-components */
//depends Screens
import React from 'react';
import {View, Text, LogBox} from 'react-native';
import {useTheme} from '@react-navigation/native';
import {NavigationContainer} from '@react-navigation/native';
import {createStackNavigator} from '@react-navigation/stack';
import {createBottomTabNavigator} from '@react-navigation/bottom-tabs';
import {createNativeStackNavigator} from '@react-navigation/native-stack';
import {createMaterialBottomTabNavigator} from '@react-navigation/material-bottom-tabs';
import MaterialCommunityIcons from 'react-native-vector-icons/MaterialCommunityIcons';
import FontAwesome from 'react-native-vector-icons/FontAwesome';
import Feather from 'react-native-vector-icons/Feather';
import Ionicons from 'react-native-vector-icons/Ionicons';
import IconAntDesign from 'react-native-vector-icons/AntDesign';

//General Colors
import {Colors} from './src/config';

//General Context
import {AppProvider} from './src/context/AppContext';

//Auth Screens
import SignUpScreen from './src/screens/Auth/SignUp';
import SignInScreen from './src/screens/Auth/SignIn';
import OnboardingScreen from './src/screens/Onboarding';
import SignInCustomerScreen from './src/screens/Auth/SignInCustomer';
import SignUpCustomerScreen from './src/screens/Auth/SignUpCustomer';
import Therme from './src/screens/Therme';
import Policy from './src/screens/Policy';

//General Screens
import HomeScreen from './src/screens/Home';
import CameraScreen from './src/screens/Camera';
import OrderScreen from './src/screens/Orders';
import MessageScreen from './src/screens/Message';
import UserIdentity from './src/screens/ChoiceType';
import ProfileScreen from './src/screens/Profile';
import OfferSent from './src/screens/OfferSent';
import OfferSuccessSent from './src/screens/OfferSuccessSent';
import OrdersDone from './src/screens/OrdersDone';
import AddScreen from './src/screens/AddScreen';
import TimelineScreen from './src/screens/TimelineScreen';
import GetPosition from './src/screens/GetPosition';

//udpate screen
import UpdateScreen from './src/modal/UpdateScreen'
import UpdatePass from './src/modal/UpdatePass'
import UpdateImg from './src/modal/UpdateImg'

//Home children view
import OfferScreen from './src/screens/OfferScreen';

//Root Screens
const Stack = createNativeStackNavigator();

//SignIn Screens Successfully
const StackSignIn = createNativeStackNavigator();

//home stack
const HomeStack = createNativeStackNavigator();

function HomeStackScreen() {
  return (
    <HomeStack.Navigator>
      <HomeStack.Screen
        name="Home"
        component={HomeScreen}
        options={{
          headerShown: false,
        }}
      />
      <HomeStack.Screen
        name="Profile"
        component={ProfileScreen}
        options={{
          headerShown: false,
        }}
      />
      <HomeStack.Screen
        name="OfferScreen"
        component={OfferScreen}
        options={{
          headerShown: false,
        }}
      />
      <HomeStack.Screen
        name="OfferSent"
        component={OfferSent}
        options={{
          headerShown: false,
        }}
      />
      <HomeStack.Screen
        name="OrderScreen"
        component={OrderScreen}
        options={{
          headerShown: false,
        }}
      />
      <HomeStack.Screen
        name="OfferSuccessSent"
        component={OfferSuccessSent}
        options={{
          headerShown: false,
        }}
      />
      <HomeStack.Screen
        name="OrdersDone"
        component={OrdersDone}
        options={{
          headerShown: false,
        }}
      />
      <HomeStack.Screen
        name="AddScreen"
        component={AddScreen}
        options={{
          headerShown: false,
        }}
      />
      <HomeStack.Screen
        name="TimelineScreen"
        component={TimelineScreen}
        options={{
          headerShown: false,
        }}
      />
      <HomeStack.Screen
        name="GetPosition"
        component={GetPosition}
        options={{
          headerShown: false,
        }}
      />
      <HomeStack.Screen
        name="UpdateScreen"
        component={UpdateScreen}
        options={{
          headerShown: false,
        }}
      />
      <HomeStack.Screen
        name="UpdateImg"
        component={UpdateImg}
        options={{
          headerShown: false,
        }}
      />
      <HomeStack.Screen
        name="UpdatePass"
        component={UpdatePass}
        options={{
          headerShown: false,
        }}
      />
    </HomeStack.Navigator>
  );
}

const SignInStack = ({route}) => {
  return (
    <StackSignIn.Navigator>
      <StackSignIn.Screen
        name="SignIn1"
        component={SignInScreen}
        options={{
          headerShown: false,
        }}
      />
      <StackSignIn.Screen
        name="MyTabs"
        component={HomeScreen}
        options={{
          headerShown: false,
        }}
      />
    </StackSignIn.Navigator>
  );
};

//Customer UserIdentity SignIn
const StackSignInCustomer = createNativeStackNavigator();
const StackCustomer = ({route}) => {
  return (
    <StackSignInCustomer.Navigator>
      <StackSignInCustomer.Screen
        name="SignInCustomer1"
        component={SignInCustomerScreen}
        options={{
          headerShown: false,
        }}
      />
      <StackSignInCustomer.Screen
        name="MyTabs"
        component={HomeScreen}
        options={{
          headerShown: false,
        }}
      />
    </StackSignInCustomer.Navigator>
  );
};

//Tabs navigationContainer with
const Tab = createBottomTabNavigator();
function MyTabs() {
  return (
    <Tab.Navigator
      screenOptions={{
        headerShown: false,
        tabBarShowLabel: false,
        tabBarStyle: {backgroundColor: Colors.lighGray, height: 50},
        tabBarInactiveTintColor: Colors.black,
        tabBarActiveTintColor: Colors.yellow,
      }}>
      <Tab.Screen
        name="HomeScreen"
        component={HomeStackScreen}
        options={{
          tabBarIcon: ({color}) => (
            <MaterialCommunityIcons name="home" color={color} size={30} />
          ),
        }}
      />
      <Tab.Screen
        name="Message"
        component={MessageScreen}
        options={{
          tabBarIcon: ({color}) => (
            <FontAwesome name="wechat" color={color} size={30} />
          ),
        }}
      />
      <Tab.Screen
        name="Camera"
        component={CameraScreen}
        options={{
          tabBarIcon: ({color}) => (
            <Feather name="camera" color={color} size={30} />
          ),
        }}
      />
      <Tab.Screen
        name="Orders"
        component={OrderScreen}
        options={{
          tabBarIcon: ({color}) => (
            <IconAntDesign name="profile" color={color} size={30} />
          ),
        }}
      />
      <Tab.Screen
        name="Profile"
        component={ProfileScreen}
        options={{
          tabBarIcon: ({color}) => (
            <FontAwesome name="user-o" color={color} size={30} />
          ),
        }}
      />
    </Tab.Navigator>
  );
}

const App = () => {
  React.useEffect(() => {
    LogBox.ignoreAllLogs();
  }, []);

  return (
    <AppProvider>
      <NavigationContainer>
        <Stack.Navigator
          initialRouteName="Onboarding"
          screenOptions={{headerShown: false}}>
          <Stack.Screen name="Onboarding" component={OnboardingScreen} />
          <Stack.Screen name="ChoiceType" component={UserIdentity} />
          <Stack.Screen name="SignUp" component={SignUpScreen} />
          <Stack.Screen name="Therme" component={Therme} />
          <Stack.Screen name="Policy" component={Policy} />
          <Stack.Screen name="SignIn" component={SignInStack} />
          <Stack.Screen
            name="SignUpCustomer"
            component={SignUpCustomerScreen}
          />
          <Stack.Screen name="SignInCustomer" component={StackCustomer} />
          <Stack.Screen name="Home" component={MyTabs} />
        </Stack.Navigator>
      </NavigationContainer>
    </AppProvider>
  );
};

export default App;
