// react-native.config.js
module.exports = {
  dependencies: {
    'some-unsupported-package': {
      platforms: {
        android: null, // disable Android platform, other platforms will still autolink if provided
      },
    },
    // 'react-native-vector-icons': {
    //   platforms: {
    //     ios: null,
    //   },
    // },
  },
};
