/* eslint-disable react-native/no-inline-styles */
import React from 'react';
import {TouchableOpacity, Text, View, Image} from 'react-native';
import {Colors, Images, Metrix, NavigationService} from '../config';

const HeaderComp = () => {
  return (
    <View style={{width: '100%'}}>
      <TouchableOpacity
        onPress={() => NavigationService.goBack()}
        style={{
          width: Metrix.HorizontalSize(48),
          height: Metrix.VerticalSize(48),
          borderRadius: 14,
          backgroundColor: Colors.placeholderGray,
          alignItems: 'center',
          justifyContent: 'center',
          marginVertical: Metrix.VerticalSize(20),
        }}>
        <Image
          source={Images.back}
          style={{
            resizeMode: 'contain',
            width: Metrix.HorizontalSize(24),
            height: Metrix.VerticalSize(24),
          }}
        />
      </TouchableOpacity>
    </View>
  );
};

export default HeaderComp;
