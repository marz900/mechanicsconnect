/* eslint-disable react-native/no-inline-styles */
import React from 'react';
import {View, Text, TouchableOpacity, Image, StyleSheet} from 'react-native';
import {Colors, CommonStyles, Images, Metrix} from '../config';

const HomeCards = ({val, onPress}) => {
  return (
    <View style={styles.container}>
      <View style={{flexDirection: 'row', alignItems: 'center'}}>
        <Image source={Images.medIcon} style={styles.imageStyle} />
        <View style={{marginLeft: Metrix.HorizontalSize(10)}}>
          <Text style={CommonStyles.textStyles.textInputText}>
            Type: {val.type}
          </Text>
          <Text style={styles.textStyle}>Used: {val.fuelUsed}</Text>
        </View>
      </View>
      <View style={{}}>
        <Text style={CommonStyles.textStyles.textInputText}>
          Price: {val.price}
        </Text>
        <TouchableOpacity
          onPress={onPress}
          style={{
            // height: 30,
            // width: 40,
            alignItems: 'center',
            justifyContent: 'center',
            borderRadius: 5,
            backgroundColor: Colors.red,
            marginTop: 5,
            paddingVertical: 5,
          }}>
          <Text style={{color: Colors.white, fontWeight: 'bold'}}>Remove</Text>
        </TouchableOpacity>
      </View>
    </View>
  );
};

const styles = StyleSheet.create({
  container: {
    backgroundColor: Colors.placeholderGray,
    flexDirection: 'row',
    justifyContent: 'space-between',
    borderRadius: 24,
    paddingVertical: Metrix.VerticalSize(10),
    paddingHorizontal: Metrix.HorizontalSize(20),
    alignItems: 'center',
    marginVertical: Metrix.VerticalSize(6),
    height: Metrix.VerticalSize(72),
  },
  imageStyle: {
    resizeMode: 'contain',
    width: Metrix.HorizontalSize(20),
    height: Metrix.VerticalSize(20),
  },
  textStyle: {
    color: Colors.introText,
    fontSize: Metrix.customFontSize(13),
    marginTop: 10,
  },
});

export default HomeCards;
