import Colors from './Colors';
import Metrix from './Metrix';

const CommonStyles = {
  textStyles: {
    heading: {
      fontWeight: 'bold',
      fontSize: Metrix.customFontSize(25),
      color: Colors.black,
    },
    intro: {
      fontSize: Metrix.customFontSize(13),
      color: Colors.black,
    },
    semiHeading: {
      fontSize: Metrix.customFontSize(15),
      color: Colors.yellow,
    },
    textInputText: {
      fontSize: Metrix.customFontSize(15),
      color: Colors.black,
    },
    buttonText: {
      fontSize: Metrix.customFontSize(17),
      color: Colors.black,
    },
  },
  container: {
    paddingHorizontal: Metrix.HorizontalSize(20),
  },
  topSvgContainer: {
    marginTop: Metrix.VerticalSize(72),
    justifyContent: 'center',
    alignItems: 'center',
  },
};

export default CommonStyles;
