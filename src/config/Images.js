export default Images = {
  logo: require('../assets/MC.png'),
  medIcon: require('../assets/medIcon.png'),
  back: require('../assets/back.png'),
  logout: require('../assets/logout.png'),
  google: require('../../src/assets/google.png'),
  facebook: require('../../src/assets/facebook.png'),
  apple: require('../../src/assets/pomme.png'),
  therme: require('../../src/assets/MechanicsConnectPrivacyPolicy.pdf'),
  policy: require('../../src/assets/MechanicsConnect-TermsAndCondition.pdf')
};
