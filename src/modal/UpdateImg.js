import React, { useState, useContext, useEffect } from 'react';

import { Alert, Dimensions , StyleSheet, TextInput, TouchableOpacity, Image, View, Text, StatusBar } from 'react-native';

//Dimensions
const screen_width = Dimensions.get('window').width
const screen_height = Dimensions.get('window').height

//firebase dependances
import firestore from '@react-native-firebase/firestore';
import storage from '@react-native-firebase/storage';
import auth from '@react-native-firebase/auth';
import {GoogleSignin, statusCodes} from '@react-native-google-signin/google-signin';

import {Colors} from '../config';
import {AppContext} from '../context/AppContext';

//supplient ressources
import Animated from 'react-native-reanimated';
import BottomSheet from 'reanimated-bottom-sheet';
import ImagePicker from 'react-native-image-crop-picker';
import Button from '../components/Button';

export default function UpdateImg({navigation}) {

  const [image, setImage] = useState('');
  const [uploading, setUploading] = useState(false);
  const [transferred, setTransferred] = useState(0);
  const { userinfos } = useContext(AppContext);

  //updating function
  const handleUpdate = async() => {
    await firestore()
      .collection('Users')
      .doc(userinfos.uid)
      .update({
        photoURL: image
      })
      .then((resp) => {
        console.log('User Updated!');
        navigation.push('Profile')
      })
      .catch((error) => { 

      })
  }

  const uploadImage = async () => {
    if( image == null ) {
      return null;
    }
    const uploadUri = image;
    let filename = uploadUri.substring(uploadUri.lastIndexOf('/') + 1);

    // Add timestamp to File Name
    const extension = filename.split('.').pop(); 
    const name = filename.split('.').slice(0, -1).join('.');
    filename = name + Date.now() + '.' + extension;

    setUploading(true);
    setTransferred(0);

    const storageRef = storage().ref(`photos/${filename}`);
    const task = storageRef.putFile(uploadUri);

    // Set transferred state
    task.on('state_changed', (taskSnapshot) => {
      console.log(
        `${taskSnapshot.bytesTransferred} transferred out of ${taskSnapshot.totalBytes}`,
      );

      setTransferred(
        Math.round(taskSnapshot.bytesTransferred / taskSnapshot.totalBytes) *
          100,
      );
    });

    try {
      await task;

      const url = await storageRef.getDownloadURL();

      setUploading(false);
      setImage(null);

      Alert.alert(
        'Image uploaded!',
        'Your image has been uploaded to the Firebase Cloud Storage Successfully!',
      );
      return url;

    } catch (e) {
      console.log(e);
      return null;
    }

  }


  const takePhotoFromCamera = () => {
    ImagePicker.openCamera({
      compressImageMaxWidth: 300,
      compressImageMaxHeight: 300,
      cropping: true,
      compressImageQuality: 0.7,
    }).then((image) => {
      console.log(image);
      const imageUri = Platform.OS === 'ios' ? image.sourceURL : image.path;
      setImage(imageUri);
    });
  };

  const choosePhotoFromLibrary = () => {
    ImagePicker.openPicker({
      width: 300,
      height: 300,
      cropping: true,
      compressImageQuality: 0.7,
    }).then((image) => {
      console.log(image);
      const imageUri = Platform.OS === 'ios' ? image.sourceURL : image.path;
      setImage(imageUri);
    });
  };

  const checkValueStatus = () => {

    console.log(input);

    if (input == '') {
      Alert.alert('TextInput is Empty, Please Enter Some Value To Continue...');
    }
    else {
      Alert.alert('Entered Value is Number.');
    }

  };

  return (
    <View style={{
        flexDirection: 'column',
        width: screen_width,
        height: screen_height,
    }}>
    <StatusBar
        barStyle="dark-content"
        translucent
        backgroundColor="rgba(0,0,0,0)"
      />

      <View style={{
        flex: 1,
        justifyContent: 'center',
        alignItems: 'center',
      }}>
        <View style={{
            justifyContent: 'center',
            alignItems: 'center',
            padding: 10
        }}>
            <Image 
            source={{uri: image}}
            style={{
                height: 100,
                width: 100,
                borderRadius: 10
            }}
            />
        </View>

            <TouchableOpacity style={styles.commandButton} onPress={() => takePhotoFromCamera()}>
                <Text style={styles.panelButtonTitle}>take image</Text>
            </TouchableOpacity>

            <TouchableOpacity style={styles.commandButton} onPress={() => choosePhotoFromLibrary()}>
                <Text style={styles.panelButtonTitle}>select image</Text>
            </TouchableOpacity>

            <TouchableOpacity style={styles.commandButton1} onPress={() => handleUpdate()}>
            <Text style={styles.panelButtonTitle}>update</Text>
            </TouchableOpacity>
      </View>
      

    </View>
  );
};

const styles = StyleSheet.create({

  MainContainer: {
    flex: 1,
    justifyContent: 'center',
    alignItems: 'center'
  },

  textInputStyle: {
    marginBottom: 20,
    textAlign: 'center',
    height: 50,
    width: '80%',
    borderWidth: 1,
    borderColor: '#4CAF50',
    borderRadius: 7,
  },
  panelButton: {
    padding: 13,
    borderRadius: 10,
    backgroundColor: '#FF6347',
    alignItems: 'center',
    marginVertical: 7,
  },
  commandButton: {
    padding: 8,
    borderRadius: 15,
    backgroundColor: '#FF6347',
    marginBottom: 30,
    width: '50%',
    alignSelf: 'center'
  },
  commandButton1: {
    padding: 8,
    borderRadius: 15,
    backgroundColor: '#FF6347',
    marginBottom: 30,
    width: '80%',
    alignSelf: 'center'
  },
  panelButtonTitle: {
    fontSize: 15,
    fontWeight: 'bold',
    color: 'white',
    textTransform: 'capitalize',
    textAlign: 'center',
  },
});