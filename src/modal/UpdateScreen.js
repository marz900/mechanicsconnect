import React, { useState, useContext, useEffect } from 'react';

import { Alert, Dimensions , StyleSheet, TextInput, TouchableOpacity, Image, View, Text, StatusBar } from 'react-native';

//Dimensions
const screen_width = Dimensions.get('window').width
const screen_height = Dimensions.get('window').height

//firebase dependances
import firestore from '@react-native-firebase/firestore';
import storage from '@react-native-firebase/storage';
import auth from '@react-native-firebase/auth';
import {GoogleSignin, statusCodes} from '@react-native-google-signin/google-signin';

import {Colors} from '../config';
import {AppContext} from '../context/AppContext';

//supplient ressources
import Animated from 'react-native-reanimated';
import BottomSheet from 'reanimated-bottom-sheet';
import ImagePicker from 'react-native-image-crop-picker';
import Button from '../components/Button';

export default function UpdateScreen({navigation}) {
  const [input, setInput] = useState('');
  const { userinfos } = useContext(AppContext);

  //updating function
  const handleUpdate = async() => {
    await firestore()
      .collection('Users')
      .doc(userinfos.uid)
      .update({
        displayName: input,
      })
      .then((resp) => {
        console.log('User Updated!');
        navigation.push('Profile')
      })
      .catch((error) => { 

      })
  }

  const checkValueStatus = () => {

    console.log(input);

    if (input == '') {
      Alert.alert('TextInput is Empty, Please Enter Some Value To Continue...');
    }else if (input1 = ''){
        Alert.alert('TextInput is Empty, Please Enter Some Value To Continue...');
    }
    else {
      Alert.alert('Entered Value is Number.');
    }

  };

  return (
    <View style={{
        flexDirection: 'column',
        width: screen_width,
        height: screen_height,
    }}>
    <StatusBar
        barStyle="dark-content"
        translucent
        backgroundColor="rgba(0,0,0,0)"
      />

      <View style={{
        flex: 1,
        justifyContent: 'center',
        alignItems: 'center',
      }}>
        <TextInput
            placeholder="enter name..."
            onChangeText={data => setInput(data)}
            style={styles.textInputStyle}
        />
            <TouchableOpacity style={styles.commandButton1} onPress={() => handleUpdate()}>
            <Text style={styles.panelButtonTitle}>update</Text>
            </TouchableOpacity>
      </View>
      

    </View>
  );
};

const styles = StyleSheet.create({

  MainContainer: {
    flex: 1,
    justifyContent: 'center',
    alignItems: 'center'
  },

  textInputStyle: {
    marginBottom: 20,
    textAlign: 'center',
    height: 50,
    width: '80%',
    borderWidth: 1,
    borderColor: '#4CAF50',
    borderRadius: 7,
  },
  panelButton: {
    padding: 13,
    borderRadius: 10,
    backgroundColor: '#FF6347',
    alignItems: 'center',
    marginVertical: 7,
  },
  commandButton: {
    padding: 8,
    borderRadius: 15,
    backgroundColor: '#FF6347',
    marginBottom: 30,
    width: '50%',
    alignSelf: 'center'
  },
  commandButton1: {
    padding: 8,
    borderRadius: 15,
    backgroundColor: '#FF6347',
    marginBottom: 30,
    width: '80%',
    alignSelf: 'center'
  },
  panelButtonTitle: {
    fontSize: 15,
    fontWeight: 'bold',
    color: 'white',
    textTransform: 'capitalize',
    textAlign: 'center',
  },
});