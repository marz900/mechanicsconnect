import {
    View,
    Text,
    StyleSheet,
    SafeAreaView,
    ScrollView,
    TouchableOpacity,
    Image,
    TextInput,
    ImageBackground,
    Alert,
    Button,
    StatusBar,
    Dimensions
  } from 'react-native';
import React, {useEffect, useState, useContext} from 'react';

//import supplient ressources
import {Avatar, Title, Caption, TouchableRipple, useTheme} from 'react-native-paper';

//import icon 
import Entypo from 'react-native-vector-icons/Entypo';
import Ionicons from 'react-native-vector-icons/Ionicons';
import FontAwesome from 'react-native-vector-icons/FontAwesome';
import MaterialIcons from 'react-native-vector-icons/MaterialIcons';
import MaterialCommunityIcons from 'react-native-vector-icons/MaterialCommunityIcons';
import Feather from 'react-native-vector-icons/Feather';
import FontAwesome5 from 'react-native-vector-icons/FontAwesome5';
import Icon from 'react-native-vector-icons/Ionicons';
import ActionButton from 'react-native-action-button';

//import firebase dependencies
import {firebase} from '@react-native-firebase/auth';
import auth from '@react-native-firebase/auth';
import firestore from '@react-native-firebase/firestore';


//External modules for...
import CustomRatingBar from '../rating/CustomeRating';

//Ressources for
import {Colors, Metrix} from '../config';

//cam dependencies
import ImagePicker from 'react-native-image-crop-picker';

// import DatePicker from 'react-datepicker';
import DatePicker from 'react-native-date-picker';
import moment from 'moment';

//import context
import {AppContext} from '../context/AppContext';

//dimensions
const {width} = Dimensions.get('screen');
const cardWith = width / 1.1;
const screen_width = Dimensions.get('window').width
const screen_height = Dimensions.get('window').height

//import picker
import { Picker } from '@react-native-picker/picker';


const AddScreen = ({navigation, route}) => {
    const [posts, setPosts] = useState(null);
    const [position, setPosition] = useState('');
    const [image, setImage] = React.useState(null);
    const [price, setPrice] = useState(' USD');
    const [description, setDescription] = useState('');
    const [date, setDate] = useState(new Date(Date.UTC(2012, 11, 20)));
    const [open, setOpen] = useState(false);
    const [dobLabel, setDobLabel] = useState('Select Date');
    const [repair, setRepair] = useState('');
    const {user, userinfos} = useContext(AppContext);
    const [input, setInput] = useState('');
    
    useEffect(() => {
        // getItemDetails()
        // console.log("kkkkkkk", user);
        // console.log("------kkkkkk-----", userinfos);
      }, []);

      const checkValueStatus = () => {
 
        console.log(input);
     
        if (input == '') {
          Alert.alert('TextInput is Empty, Please Enter Some Value To Continue...');
        }
        else if (isNaN(input)) {
          Alert.alert('Entered Value NOT is Number.');
        }
        else {
          Alert.alert('Entered Value is Number.');
        }
     
      };

      const takePhotoFromCamera = () => {
        ImagePicker.openCamera({
          width: 1200,
          height: 780,
          cropping: true,
        }).then((image) => {
          console.log(image);
          const imageUri = Platform.OS === 'ios' ? image.sourceURL : image.path;
          setImage(imageUri);
        });
      };

      const choosePhotoFromLibrary = () => {
        ImagePicker.openPicker({
          width: 1200,
          height: 780,
          cropping: true,
        }).then((image) => {
          console.log(image);
          const imageUri = Platform.OS === 'ios' ? image.sourceURL : image.path;
          setImage(imageUri);
        });
      };

      const submitPost = async () => {
        const imageUrl = await uploadImage();
        console.log('Image Url: ', imageUrl);
        console.log('Post: ', posts);
    
        firestore()
        .collection('Posts')
        .add({
          uid: user.uid,
          post: posts,
          postImg: imageUrl,
          postTime: firestore.Timestamp.fromDate(new Date()),
          likes: null,
          comments: null,
        })
        .then(() => {
          console.log('Post Added!');
          Alert.alert(
            'Post published!',
            'Your post has been published Successfully!',
          );
          setPost(null);
        })
        .catch((error) => {
          console.log('Something went wrong with added post to firestore.', error);
        });
      }

      const SendData = () => {
        return (
          <ActionButton buttonColor={Colors.yellow}>
            <ActionButton.Item
              buttonColor="#9b59b6"
              title="Take Photo"
              onPress={takePhotoFromCamera}>
              <Icon name="camera-outline" style={styles.actionButtonIcon} />
            </ActionButton.Item>
            <ActionButton.Item
              buttonColor="#3498db"
              title="Choose Photo"
              onPress={choosePhotoFromLibrary}>
              <Icon name="md-images-outline" style={styles.actionButtonIcon} />
            </ActionButton.Item>
          </ActionButton>
        )
      }

      const uploadImage = async () => {
        if( image == null ) {
          return null;
        }
        const uploadUri = image;
        let filename = uploadUri.substring(uploadUri.lastIndexOf('/') + 1);
    
        // Add timestamp to File Name
        const extension = filename.split('.').pop(); 
        const name = filename.split('.').slice(0, -1).join('.');
        filename = name + Date.now() + '.' + extension;
    
        setUploading(true);
        setTransferred(0);
    
        const storageRef = storage().ref(`photos/${filename}`);
        const task = storageRef.putFile(uploadUri);
    
        // Set transferred state
        task.on('state_changed', (taskSnapshot) => {
          console.log(
            `${taskSnapshot.bytesTransferred} transferred out of ${taskSnapshot.totalBytes}`,
          );
    
          setTransferred(
            Math.round(taskSnapshot.bytesTransferred / taskSnapshot.totalBytes) *
              100,
          );
        });
    
        try {
          await task;
    
          const url = await storageRef.getDownloadURL();
    
          setUploading(false);
          setImage(null);
    
          // Alert.alert(
          //   'Image uploaded!',
          //   'Your image has been uploaded to the Firebase Cloud Storage Successfully!',
          // );
          return url;
    
        } catch (e) {
          console.log(e);
          return null;
        }
    
      };
      
      const handleAdd = async () => {  
        try {
          await firestore()
            .collection('services')
            .add({
              uid: auth().currentUser.uid,
              userName: userinfos.displayName,
              postImg: image,
              price: price,
              description: description,
              position: position,
              date: dobLabel,
              repair: repair,
              postTime: firestore.FieldValue.serverTimestamp(),
            })
            .then(() => {
              navigation.push('Home')
              console.log('Post Added!');
            //   Alert.alert('Post published!', 'Your post has been published Successfully!');
            })
            .catch(e => {
              console.log(e);
            });
        } catch (error) {
          console.log('first', error);
        }
      };


    
    
  return (
    <><View style={{
      width: screen_width,
      height: screen_height,
      flexDirection: 'column'
    }}>
      <ScrollView nestedScrollEnabled={true} showsVerticalScrollIndicator={false}>
      <StatusBar
        barStyle="light-content"
        translucent
        backgroundColor="rgba(0,0,0,0)" />
      
        <View style={{
          flex: 0.5,
          justifyContent: 'center',
          alignItems: 'center',
        }}>
          <Image
            source={{ uri: image }}
            style={{
              height: 100,
              width: '100%',
              borderRadius: 10,
            }} />
        </View>
        <View style={styles.MainContainer}>
          <TextInput
            underlineColorAndroid="transparent"
            autoCapitalize='sentences'
            keyboardType='default'
            autoFocus={true}
            placeholder="problem type"
            onChangeText={repair => setRepair(repair)}
            style={styles.textInputStyle} />
          <TextInput
            underlineColorAndroid="transparent"
            autoCapitalize='none'
            autoFocus={true}
            keyboardType='default'
            placeholder="enter description here"
            onChangeText={description => setDescription(description)}
            style={styles.textInputStyle} />
          <TextInput
            underlineColorAndroid="transparent"
            autoCapitalize='none'
            autoFocus={true}
            placeholder="enter your location..."
            onChangeText={position => setPosition(position)}
            style={styles.textInputStyle} />

          <View style={styles.action}>
          <FontAwesome name="calendar-o" color={Colors.yellow} size={20} />
          <TouchableOpacity onPress={() => setOpen(true)}>
            <Text style={{ color: '#666', marginLeft: 5, marginTop: 5 }}>{dobLabel}</Text>

            <DatePicker
              modal
              open={open}
              date={date}
              format="YYYY-MM-DD"
              placeholder="select date"
              mode={'date'}
              maximumDate={new Date('2030-01-01')}
              minimumDate={new Date('2022-01-01')}
              onConfirm={date => {
                setOpen(false);
                setDate(date);
                setDobLabel(date.toLocaleDateString());
              } }
              onCancel={() => {
                setOpen(false);
              } } />
          </TouchableOpacity>
          </View>
          <TextInput
            underlineColorAndroid="transparent"
            autoCapitalize='none'
            autoFocus={true}
            keyboardType='number-pad'
            placeholder="add price"
            onChangeText={price => setPrice(price)}
            style={styles.textInputStyle} />
          <TouchableOpacity style={{ backgroundColor: Colors.green, padding: 14, borderRadius: 13 }} onPress={() => handleAdd() }>
            <Text
              style={{
                textTransform: 'uppercase',
                fontSize: 13,
                fontStyle: 'normal',
                fontFamily: 'Roboto',
                fontWeight: 'bold',
                color: Colors.white,
              }}>
              add service
            </Text>
          </TouchableOpacity>
        </View>
      </ScrollView>
    </View><SendData /></>
    
  )
}

export default AddScreen

const styles = StyleSheet.create({
    container: {
      flex: 1,
    },
    Uniform1: {
      fontSize: 15,
      fontWeight: 'bold',
      fontStyle: 'normal',
      paddingVertical: '3%',
    },
    commandButton: {
      padding: 13,
      borderRadius: 20,
      backgroundColor: Colors.yellow,
      marginTop: 5,
      width: '50%',
      alignSelf: 'center',
    },
    textInput: {
      flex: 1,
      marginTop: Platform.OS === 'ios' ? 0 : -12,
      paddingLeft: 10,
      color: Colors.black,
      bottom: 5,
      width: '80%',
      padding: -2
  
    },
    MainContainer: {
      flex: 2,
      alignItems: 'center',
      flexDirection: 'column',
    },
    action: {
      marginBottom: 20,
      height: 50,
      width: '90%',
      borderWidth: 1.5,
      borderColor: '#4CAF50',
      borderRadius: 7,
      fontStyle: 'italic',
      paddingHorizontal: 10,
    },
  
    textInputStyle: {
      marginBottom: 20,
      height: 50,
      width: '90%',
      borderWidth: 1.5,
      borderColor: '#4CAF50',
      borderRadius: 7,
      fontStyle: 'italic',
      paddingHorizontal: 10,
    },
    textInputText: {
      color: Colors.textDarkGray,
      fontSize: Metrix.customFontSize(14),
      textTransform: 'capitalize',
    },
  });
  