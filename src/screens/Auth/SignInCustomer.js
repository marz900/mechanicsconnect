/* eslint-disable react-native/no-inline-styles */
import React, {useState, useEffect, useContext} from 'react';
import {
  View,
  Text,
  StyleSheet,
  Image,
  TouchableOpacity,
  ScrollView,
  Platform,
  Alert,
  Keyboard,
  ToastAndroid,
  ActivityIndicator,
} from 'react-native';
import {Button, TextInputComp} from '../../components';
import {Colors, Images, Metrix, NavigationService} from '../../config';

//firebase configurations...
import firestore, { firebase } from '@react-native-firebase/firestore';
import { GoogleSignin } from '@react-native-google-signin/google-signin';
import auth from '@react-native-firebase/auth';

//import context
import { AppContext } from '../../context/AppContext';


const SignInScreen = ({navigation}) => {
  const [email, setEmail] = useState('');
  const [password, setPassword] = useState('');
  const [secure, setSecure] = useState(true);
  const {user, setUser, userinfos} = useContext(AppContext);
  const [show, setShow] = useState(false);

  useEffect(() => {
    console.log('first11113-----', user);
    console.log('userinfos', userinfos);
  }, [])

  //connect with use google...
  const handleLogin = async () => {
    try {
      auth()
        .signInWithEmailAndPassword(email, password)
        .then(userCredentials => {
          const user = userCredentials.user;
          console.log('user: ', user)
          setUser(user);
          if(user) {
            setShow(true)
            setTimeout(() => {
              setShow(false)
              // console.debug('User data', user);
              navigation.navigate('Home');
              // Alert.alert("Welcome to MC home 🖐!");
            }, 2000);
            
          }
        }).catch((error) => {
          if (error.code === 'auth/invalid-email') {
            ToastAndroid.show('invalid email address', ToastAndroid.SHORT);
          }
          if(error.code === 'auth/wrong-password') {
            ToastAndroid.show('wrong password !', ToastAndroid.SHORT);
          }
        });
    } catch (error) {
      console.log(error)
    }
  };

  const validate = async () => {
    Keyboard.dismiss();
    let isValid = true;

    if (!email) {
      ToastAndroid.show('Please input email !', ToastAndroid.SHORT);
      isValid = false;
    } else if (!email.match(/\S+@\S+\.\S+/)) {
      ToastAndroid.show('Please input a valid email !', ToastAndroid.SHORT);
      isValid = false;
    }

    if (!password) {
      ToastAndroid.show('Please input password', ToastAndroid.SHORT);
      isValid = false;
    } else if (password.length < 5) {
      ToastAndroid.show('Min password length of 5', ToastAndroid.SHORT);
      isValid = false;
    }
    if (isValid) {
      handleLogin();
    }
  };


  //connect with use google account
  const onGoogleButtonPress = async () => {
    const {idToken} = await GoogleSignin.signIn();
    const googleCredential = auth.GoogleAuthProvider.credential(idToken);
    return auth().signInWithCredential(googleCredential);
  };

  const onAuthStateChanged = async userAuth => {
    if (!userAuth) {
      return;
    }
    if (userAuth) {
      console.log('indicatif----', userAuth);
      setUser(userAuth);
      if (user) {
        const payload = {
          uid: userAuth.uid,
          email: userAuth.email,
          displayName: userAuth.displayName,
          photoURL: userAuth.photoURL,
          postTime: firestore.FieldValue.serverTimestamp(),
        };

          firestore()
          .collection('Users')
          .doc(auth().currentUser.uid)
          .set(payload)
          .then(res => {
            if (res != 0) {
              navigation.navigate('Home');
            }
          })
          .catch(e => {
            console.log('ERRR', e);
          });
        }
    }

    return () => userReference();
  };

  // useEffect(() => {
  //   const subscriber = auth().onAuthStateChanged(onAuthStateChanged);
  //   return () => {
  //     subscriber;
  //   };
  // }, [])

  return (
    <ScrollView style={styles.container}>
      <View style={styles.secondContainer}>
        <Image source={Images.logo} style={styles.logoImage} />
      </View>
      <View style={{marginVertical: Metrix.VerticalSize(30)}}>
        <Text style={styles.welcomeText}>Welcome to MC</Text>
        <Text style={styles.signinText}>Mobile Mechanic Services</Text>
      </View>
      <View style={{flex: 1, marginBottom: Metrix.VerticalSize(50)}}>
        <View
          style={{
            height: Metrix.VerticalSize(50),
            marginBottom: Metrix.VerticalSize(10),
          }}>
          <Text style={styles.textInputText}>Email Address</Text>
          <TextInputComp
            onChange={text => setEmail(text)}
            placeholder={'Enter Email'}
            type={'email-address'}
          />
        </View>
        <View style={{height: Metrix.VerticalSize(30), marginTop: 40}}>
          <Text style={styles.textInputText}>Password</Text>
          <View style={{flexDirection: 'row', height: Metrix.VerticalSize(50)}}>
            <TextInputComp
              onChange={text => setPassword(text)}
              placeholder={'Enter Password'}
              secure={secure}
              secureWidth={true}
            />
            <TouchableOpacity
              style={{
                alignSelf: 'flex-end',
                paddingHorizontal: Platform.OS === 'ios' ? 5 : 2,
              }}
              onPress={() => setSecure(!secure)}>
              {secure ? (
                <Text style={{marginVertical: 10, ...styles.textInputText1}}>
                  show
                </Text>
              ) : (
                <Text style={{marginVertical: 10, ...styles.textInputText}}>
                  Hide
                </Text>
              )}
            </TouchableOpacity>
          </View>
        </View>
      </View>
      <View style={{
        paddingTop: 3,
          alignItems: 'flex-end',
        }}>
          <TouchableOpacity>
            <Text style={{
              textTransform: 'capitalize',
              fontSize: 13,
              fontWeight: 'bold',
              fontStyle: 'italic',
              fontFamily: 'Roboto',
            }}>
              forgot password
            </Text>
          </TouchableOpacity>
        </View>
      <View
        style={{
          width: '100%',
          height: Metrix.VerticalSize(60),
          marginVertical: Metrix.VerticalSize(30),
        }}>
        <Button
          color={Colors.black}
          textColor={Colors.white}
          onPress={() => validate()}
          title={'Sign In'}
        />
      </View>

      <View
        style={{
          alignSelf: 'center',
          flexDirection: 'row',
        }}>
        <View style={[styles.icon, {}]}>
          <TouchableOpacity onPress={() => onGoogleButtonPress()}>
            <Image
              source={Images.google}
              style={{
                width: 45,
                height: 45,
              }}
            />
          </TouchableOpacity>
        </View>
        <View style={[styles.icon, {}]}>
          <TouchableOpacity onPress={() => {}}>
            <Image
              source={Images.apple}
              style={{
                width: 45,
                height: 45,
              }}
            />
          </TouchableOpacity>
        </View>
        <View style={[styles.icon, {}]}>
          <TouchableOpacity onPress={() => {}}>
            <Image
              source={Images.facebook}
              style={{
                width: 45,
                height: 45,
              }}
            />
          </TouchableOpacity>
        </View>
      </View>

      <View style={{flex: 1, padding: 25}}>
        <View style={{flexDirection: 'row', justifyContent: 'center'}}>
          <Text
            style={[
              styles.textInputText,
              {color: Colors.black},
            ]}>
            Don't have an account?{' '}
          </Text>
          <TouchableOpacity onPress={() => navigation.navigate('SignUpCustomer')}>
            <Text style={styles.resetText}>Sign Up</Text>
          </TouchableOpacity>
        </View>
      </View>
      <ActivityIndicator size="large" color={Colors.red} animating={show} />
    </ScrollView>
  );
};

const styles = StyleSheet.create({
  container: {
    flex: 1,
    backgroundColor: Colors.white,
    paddingHorizontal: Metrix.HorizontalSize(35),
  },
  secondView: {
    marginTop: Metrix.VerticalSize(90),
  },
  secondContainer: {
    alignItems: 'center',
    justifyContent: 'center',
    paddingTop: Metrix.VerticalSize(90),
  },
  logoImage: {
    resizeMode: 'contain',
    width: Metrix.HorizontalSize(149),
    height: Metrix.VerticalSize(126),
    marginBottom: Metrix.VerticalSize(10),
  },
  welcomeText: {
    color: Colors.yellow,
    fontSize: Metrix.customFontSize(18),
    fontWeight: 'bold',
    marginBottom: 10,
    textAlign: 'center',
  },
  signinText: {
    color: Colors.yellow,
    fontSize: Metrix.customFontSize(12),
    fontWeight: 'bold',
    textAlign: 'center',
  },
  textInputText: {
    color: Colors.textDarkGray,
    fontSize: Metrix.customFontSize(14),
  },
  textInputText1: {
    color: Colors.textDarkGray,
    fontSize: Metrix.customFontSize(10),
  },
  resetText: {
    color: Colors.yellow,
    fontSize: Metrix.customFontSize(14),
  },
  icon: {
    marginHorizontal: '4%',
  },
});

export default SignInScreen;
