/* eslint-disable react-native/no-inline-styles */
import React, {useEffect, useState} from 'react';
import {View, Text, StyleSheet, Dimensions, TouchableOpacity, Image, Platform, StatusBar, ScrollView, Keyboard, ToastAndroid, ActivityIndicator, Alert} from 'react-native';
import {Button, TextInputComp} from '../../components';
import {Colors, Metrix,Images} from '../../config';
import Pdf from 'react-native-pdf';

//firebase configurations...
import auth from '@react-native-firebase/auth';
import firestore from '@react-native-firebase/firestore';
import {firebase} from '@react-native-firebase/auth';

// import File from '../PdfFile';

const SignUpScreen = ({navigation}) => {
  const [email, setEmail] = useState('');
  const [name, setName] = useState('');
  const [password, setPassword] = useState('');
  const [confirmPassword, setConfirmPassword] = useState('');
  const [secure, setSecure] = useState(true);
  const [secureCon, setSecureCon] = useState(true);
  const [show, setShow] = useState(false);

  useEffect(() => {
    console.log('name enterrrrrr', name)
    console.log('name enterrrrrr', password)
    console.log('name enterrrrrr', email)
  }, [name,email,password])


  const File = () => {
    const source = {uri: 'http://samples.leanpub.com/thereactnativebook-sample.pdf', cache: true } ;
    const source1 = Images.policy;
    console.log('titititi', <Pdf/>)
    //const source = require('./test.pdf');  // ios only
    //const source = {uri:'bundle-assets://test.pdf' };
    //const source = {uri:'file:///sdcard/test.pdf'};
    //const source = {uri:"data:application/pdf;base64,JVBERi0xLjcKJc..."};
    //const source = {uri:"content://com.example.blobs/xxxxxxxx-...?offset=0&size=xxx"};
    //const source = {uri:"blob:xxxxxxxx-...?offset=0&size=xxx"};

    return (
            <Pdf
                source={source}
                onLoadComplete={(numberOfPages,filePath) => {
                    console.log(`Number of pages: ${numberOfPages}`);
                }}
                onPageChanged={(page,numberOfPages) => {
                    console.log(`Current page: ${page}`);
                }}
                onError={(error) => {
                    console.log(error);
                }}
                onPressLink={(uri) => {
                    console.log(`Link pressed: ${uri}`);
                }}
                style={styles.pdf}/>
    )
}
  //main function
  const Register = async () => {
    try {
      const userCreate = await firebase  
        .auth()
        .createUserWithEmailAndPassword(email, password);
        firestore()
        .collection('Users')
        .doc(userCreate.user.uid)
        .set({
          uid: userCreate.user.uid,
          displayName: name,
          email: email,
          photoURL: null,
          status: 'mechanic',
          postTime: firestore.FieldValue.serverTimestamp(),
        })
        .then(() => {
          setShow(true)
          setTimeout(() => {
            setShow(true)
            navigation.navigate('SignIn');
            Alert.alert("successful registration !");
            console.log('successful customer add');
          }, 2000);
        });
        console.log('first11113', userCreate.user)
    } catch (error) {
      console.log('first11113', error);
    }
  }

  const validate = async () => {
    Keyboard.dismiss();
    let isValid = true;

    if (!email) {
      ToastAndroid.show('Please input email !', ToastAndroid.SHORT);
      isValid = false;
    } else if (!email.match(/\S+@\S+\.\S+/)) {
      ToastAndroid.show('Please input a valid email !', ToastAndroid.SHORT);
      isValid = false;
    }
    if (!name) {
      ToastAndroid.show('Please input fullname', ToastAndroid.SHORT);
      isValid = false;
    }

    if (!password) {
      ToastAndroid.show('Please input password', ToastAndroid.SHORT);
      isValid = false;
    } else if (password.length < 5) {
      ToastAndroid.show('Min password length of 5', ToastAndroid.SHORT);
      isValid = false;
    }
    if (!confirmPassword) {
      ToastAndroid.show('Please input password', ToastAndroid.SHORT);
      isValid = false;
    } else if (confirmPassword.length < 5) {
      ToastAndroid.show('Min password length of 5', ToastAndroid.SHORT);
      isValid = false;
    }

    if (confirmPassword !== password) {
      ToastAndroid.show('password and confirmPassword must be identical', ToastAndroid.SHORT);
      isValid = false;
    }

    if (isValid) {
      Register();
    }
  };

  
  

  return (
    <View style={styles.container}>
      <ScrollView showsVerticalScrollIndicator={false}>
      <StatusBar backgroundColor={Colors.white} barStyle="dark-content" />
       <View style={styles.secondContainer}>
        <Image source={Images.logo} style={styles.logoImage} />
      </View>  
      <View style={{marginVertical: Metrix.VerticalSize(10), marginTop: 10}}>
        <Text style={styles.textInputText}>Full Name</Text>
        <View style={styles.textInputView}>
          <TextInputComp
            value={name}
            onChange={text => setName(text)}
            placeholder={'Full Name'}
          />
        </View>
        <View style={{marginVertical: Metrix.VerticalSize(10), marginTop: 10}}>
        <Text style={styles.textInputText}>Email Address</Text>
        <View style={styles.textInputView}>
          <TextInputComp
            value={email}
            onChange={text => setEmail(text)}
            placeholder={'Email Address'}
            type={'email-address'}
          />
        </View>
        </View>
        <View style={{marginVertical: Metrix.VerticalSize(10), marginTop: 10}}>
        <Text style={styles.textInputText}>Password</Text>
        <View style={{flexDirection: 'row', ...styles.textInputView}}>
          <TextInputComp
            value={password}
            onChange={text => setPassword(text)}
            placeholder={'Password'}
            secure={secure}
            secureWidth={true}
          />
          <TouchableOpacity
            style={{alignSelf: 'flex-end'}}
            onPress={() => setSecure(!secure)}>
            {secure ? (
              <Text
                style={{
                  marginVertical: 10,
                  ...styles.textInputText1,
                }}>
                Show
              </Text>
            ) : (
              <Text
                style={{
                  marginVertical: 10,
                  ...styles.textInputText,
                }}>
                Hide
              </Text>
            )}
          </TouchableOpacity>
        </View>
        </View>
        <View style={{marginVertical: Metrix.VerticalSize(30), marginTop: 10}}>
        <Text style={styles.textInputText}>Confirm Password</Text>
        <View style={{flexDirection: 'row', ...styles.textInputView}}>
          <TextInputComp
            value={confirmPassword}
            onChange={text => setConfirmPassword(text)}
            placeholder={'Confirm Password'}
            secure={secureCon}
            secureWidth={true}
          />
          <TouchableOpacity
            style={{alignSelf: 'flex-end'}}
            onPress={() => setSecureCon(!secureCon)}>
            {secureCon ? (
              <Text
                style={{
                  marginVertical: 10,
                  ...styles.textInputText,
                }}>
                Show
              </Text>
            ) : (
              <Text
                style={{
                  marginVertical: 10,
                  ...styles.textInputText,
                }}>
                Hide
              </Text>
            )}
          </TouchableOpacity>
        </View>
        </View>
      </View>
      <View
        style={{
          height: Metrix.VerticalSize(60),
          // marginVertical: Metrix.VerticalSize(5),
        }}>
        <Button
          color={Colors.black}
          onPress={() => validate()}
          textColor={Colors.white}
          title={'Sign Up'}
        />
      </View>
      <View
        style={{
          flexDirection: 'row',
          alignItems: 'center',
          justifyContent: 'center',
          padding: 5
        }}>
        <Text style={[styles.textInputText, {fontSize: Metrix.customFontSize(14), color: Colors.black,}]}>Already have an account? </Text>
        <TouchableOpacity onPress={() => navigation.navigate('SignIn')}>
          <Text style={styles.resetText}>Sign In</Text>
        </TouchableOpacity>
      </View>
      <View 
        style={{
          flexDirection: 'column',
          alignItems: 'center',
          justifyContent: 'space-evenly',
          padding: Platform.OS === 'android' ? 5 : 10,
          width: '100%'
        }}>
          <Text style={[styles.themeCustom, {}]}>All users are subjected to our Terms &</Text>
              <Text style={[styles.termeCustom, {}]} onPress={() => navigation.navigate('Therme')}>conditions{'\t'}<Text style={[styles.themeCustom, {}]}>Privacy &</Text>
              <Text style={[styles.termeCustom, {}]} onPress={() => navigation.navigate('Policy')}>{'\t'}Policy</Text></Text>
      </View>
      </ScrollView>
      <ActivityIndicator size="large" color={Colors.red} animating={show} />
    </View>
  );
};

const styles = StyleSheet.create({
  container: {
    flex: 1,
    backgroundColor: Colors.white,
    paddingHorizontal: Metrix.HorizontalSize(35),
  },
  secondView: {
    marginTop: Metrix.VerticalSize(90),
  },
  welcomeText: {
    color: Colors.black,
    fontSize: Metrix.customFontSize(25),
    fontWeight: 'bold',
    marginBottom: 10,
  },
  textInputText: {
    color: Colors.textDarkGray,
    fontSize: Metrix.customFontSize(12),
  },
  textInputText1: {
    color: Colors.textDarkGray,
    fontSize: Metrix.customFontSize(12),
  },
  signinText: {
    color: Colors.textGray,
    fontSize: Metrix.customFontSize(14),
  },
  textInputView: {
    height: Metrix.VerticalSize(50),
    marginVertical: Metrix.VerticalSize(10),
  },
  resetText: {
    color: Colors.yellow,
    fontSize: Metrix.customFontSize(14),
  },
  secondContainer: {
    alignItems: 'center',
    justifyContent: 'center',
    paddingTop: Platform.OS === 'ios' ? Metrix.VerticalSize(90) : Metrix.VerticalSize(30),
  },
  logoImage: {
    resizeMode: 'contain',
    width: Metrix.HorizontalSize(150),
    height: Metrix.VerticalSize(130),
    marginBottom: Metrix.VerticalSize(10),
  },
  themeCustom: {
    fontSize: Metrix.customFontSize(14), 
    color: Colors.black,
    textAlign: 'center'
    
  },
  termeCustom: {
    fontSize: Metrix.customFontSize(14),
    color: Colors.blue,
    textAlign: 'center'
  },
  container22: {
    flex: 1,
    justifyContent: 'flex-start',
    alignItems: 'center',
    marginTop: 25,
},
pdf: {
    flex:1,
    width:Dimensions.get('window').width,
    height:Dimensions.get('window').height,
}
});

export default SignUpScreen;
