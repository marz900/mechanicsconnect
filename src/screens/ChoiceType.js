import { View, Text, Image, StyleSheet, Dimensions, Platform, TouchableOpacity } from 'react-native'
import React, {useState, useEffect, useContext} from 'react'
import {Colors, Images, Metrix, NavigationService} from '../config';
const { width, height } = Dimensions.get('screen')

const UserIdentity = ({ navigation, route }) => {
  return (
      <View style={styles.container}>
          <View style={[styles.image, { flex: 3}]}>
                <Image source={require('../assets/user-mechanic.jpg')} style={styles.image} />
          </View>
          <View style={{ flex: 3}}>
              <View>
                  <TouchableOpacity onPress={() => navigation.push('SignIn')} style={[styles.buttonChoice, {marginBottom: 20}]}>
                    <Text style={[styles.textCustom]}>I'am mechanic</Text>
                  </TouchableOpacity>
             </View>
            <View>
                <TouchableOpacity onPress={() => navigation.push('SignInCustomer')} style={styles.buttonChoice}>
                    <Text style={styles.textCustom}>I'am customer</Text>
                </TouchableOpacity>
            </View>
            </View>
    </View>
  )
}
export default UserIdentity;

const styles = StyleSheet.create({
    container: {
        flex: 1,
        flexDirection: 'column',
        backgroundColor: Colors.grey,
        width: width,
        height: height,

    },
    textCustom: {
        fontSize: 18,
        textAlign: 'center',
        


    },
    image: {
        width: 150,
        height: 150,
        justifyContent: 'center',
        alignSelf: 'center',
        borderRadius: 15,
    },
    buttonChoice: {
        justifyContent: 'center',
        backgroundColor: Colors.yellow,
        padding: 8,
        borderRadius: Platform.OS === 'ios'? 10 : 20,
        borderWidth: 2,
        borderColor: Colors.yellow,
        width: '50%',
        alignSelf: 'center',
    }
})