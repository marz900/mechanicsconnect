/* eslint-disable react-hooks/exhaustive-deps */
/* eslint-disable react-native/no-inline-styles */
import {View, Text, Alert, Platform, NativeModules} from 'react-native';
import React, {useEffect, useState} from 'react';
import {Colors, CommonStyles, Metrix, NavigationService} from '../config';
import HeaderComp from '../components/HeaderComp';
import SelectDropdown from 'react-native-select-dropdown';
import {Button, TextInputComp} from '../components';
import {useDispatch, useSelector} from 'react-redux';
import AsyncStorage from '@react-native-async-storage/async-storage';
import AuthAction from '../redux/Actions/AuthAction';
import CalendarModule from '../../CalendarModule';
import DeviceInfo from 'react-native-device-info';

const CreateList = () => {
  const data = ['Petrol', 'Diesel', 'Battery Charge'];
  const [selected, setSelected] = useState('');
  const [litres, setLitres] = useState('');
  const [allowance, setAllowance] = useState('');
  const [deviceId, setDeviceId] = useState('');
  const [brand, setBrand] = useState('');
  const [manufacturer, setManufacturer] = useState('');
  const [devicename, setDeviceName] = useState('');
  const [board, setBoard] = useState('');
  const [type, setType] = useState('');
  const [builderId, setBuilderId] = useState('');
  const [builder, setBuilder] = useState('');
  const [release, setRelease] = useState('');
  const [versionbuilder, setBuilderVersion] = useState('');
  const [buildernumber, setBuilderNumber] = useState('');
  const [dataN, setDataN] = useState([]);
  const {CustomArray} = NativeModules;

  const dispatch = useDispatch();
  const fuelStore = useSelector(state => state.AuthReducer.fuelCharges);
  const fuelCards = useSelector(state => state.AuthReducer.fuelCards);

  useEffect(() => {
    getAsyncData();
    fetchDeviceId();
    getConstantDeviceInfo();
    JSONOBJET();
    // console.log('verification', builderId);
    // console.log('first1', brand);
    // console.log('first2', manufacturer);
    // console.log('first3', devicename);
    // console.log('first9', version);
    // console.log('first8', builder);
    // console.log('first4', type);
    // console.log('first4', release);
    // console.log('first4', board);
    // console.log('first00000', buildernumber);
    // console.log('device iofo', JSON.parse(dataN));
  }, []);

  const getConstantDeviceInfo = () => {
    let deviceJSON = {};

    deviceJSON.deviceId = DeviceInfo.getDeviceId();
    setDeviceId(deviceJSON.deviceId);
    deviceJSON.bundleId = DeviceInfo.getBundleId();
    setBuilderId(deviceJSON.bundleId);
    deviceJSON.systemName = DeviceInfo.getSystemName();
    deviceJSON.systemVersion = DeviceInfo.getSystemVersion();
    setRelease(deviceJSON.systemVersion);
    deviceJSON.version = DeviceInfo.getVersion();
    deviceJSON.readableVersion = DeviceInfo.getReadableVersion();
    deviceJSON.buildNumber = DeviceInfo.getBuildNumber();
    setBuilderNumber(deviceJSON.buildNumber);
    deviceJSON.isTablet = DeviceInfo.isTablet();
    deviceJSON.appName = DeviceInfo.getApplicationName();
    deviceJSON.brand = DeviceInfo.getBrand();
    deviceJSON.model = DeviceInfo.getModel();
    setDeviceName(deviceJSON.model);
    deviceJSON.deviceType = DeviceInfo.getDeviceType();
    setType(deviceJSON.deviceType);
    deviceJSON.manufacturer = DeviceInfo.getManufacturer();
    setManufacturer(deviceJSON.manufacturer);
    return deviceJSON;
  };

  const getAsyncData = async () => {
    try {
      const value = await AsyncStorage.getItem('@userMaxAllowance');
      if (value !== null) {
        setAllowance(value);
      }
    } catch (e) {
      // error reading value
    }
  };

  const JSONOBJET = async () => {
    try {
      const obj = await CustomArray.GetConstants();
      setDataN(obj);
    } catch (e) {
      console.log(e);
    }
  };

  const fetchDeviceId = async () => {
    try {
      const id = await CalendarModule.getDeviceId();
      setDeviceId(id);
      const boardName = await CalendarModule.getBoard();
      setBoard(boardName);
      const manufacturerName = await CalendarModule.getManufacturer();
      setManufacturer(manufacturerName);
      const brandName = await CalendarModule.getBrand();
      setBrand(brandName);
      const name = await CalendarModule.getDeviceName();
      setDeviceName(name);
      const typeSys = await CalendarModule.getType();
      setType(typeSys);
      const versionSys = await CalendarModule.getVersionSystem();
      setBuilderId(versionSys);
      const builderId = await CalendarModule.getBuilderId();
      setBuilderId(builderId);
      const releaseId = await CalendarModule.getVersionRelease();
      setRelease(releaseId);
      const versionbuilder = await CalendarModule.getBuilderNumber();
      setBuilderVersion(versionbuilder);
      const builderNumber = await CalendarModule.getBuilderVersion();
      setBuilderNumber(builderNumber);
    } catch (e) {
      // error reading value
    }
  };

  //my device infos function
  const getDeviceInfo = async () => {
    let currentPlatform = Platform.OS;
    let isValue = false;
    if (currentPlatform === 'android') {
      isValue = true;
      console.log(Platform.OS);
      console.log(isValue);
      const infoPlatform = {
        UniqueId: deviceId,
        brand: brand,
        deviceType: type,
        manufacturer: manufacturer,
        model: devicename,
        Builder: builder,
        buildNumber: buildernumber,
        systemVersion: release,
        bundleId: builderId,
      };
      try {
        console.log('first---1111----', infoPlatform);
        NavigationService.navigate('DeviceType', infoPlatform);
      } catch (e) {}
    } else if (currentPlatform === 'ios') {
      isValue = true;
      console.log(Platform.constants.Manufacturer);
      const infoPlatform = {
        UniqueId: deviceId,
        deviceType: type,
        manufacturer: manufacturer,
        model: devicename,
        Builder: builder,
        buildNumber: buildernumber,
        systemVersion: release,
        bundleId: builderId,
      };
      try {
        NavigationService.navigate('DeviceType', infoPlatform);
      } catch (e) {}
    } else {
      isValue = false;
      console.log(Platform.constants.Manufacturer);
      currentPlatform = Platform.OS;
    }
  };

  const onSubmit = async () => {
    if (selected === 'Petrol') {
      const fuelPrice = fuelStore[0].pricePerLiter * litres;
      const newAllowance = allowance - fuelPrice;
      if (fuelPrice > allowance) {
        Alert.alert('Sorry!', "You don't have enough fuel allowance");
      } else {
        console.warn(newAllowance);
        let cardData = {
          type: selected,
          fuelUsed: litres,
          price: JSON.stringify(fuelPrice),
        };
        console.warn('card', cardData);
        dispatch(AuthAction.MyFuelCards([...fuelCards, cardData]));
        try {
          await AsyncStorage.setItem(
            '@userMaxAllowance',
            JSON.stringify(newAllowance),
          );
        } catch (e) {
          console.warn(e);
        }
        NavigationService.navigate('Home');
      }
    } else if (selected === 'Diesel') {
      const fuelPrice = fuelStore[1].pricePerLiter * litres;
      const newAllowance = allowance - fuelPrice;
      if (fuelPrice > allowance) {
        Alert.alert('Sorry!', "You don't have enough fuel allowance");
      } else {
        console.warn(newAllowance);
        let cardData = {
          type: selected,
          fuelUsed: litres,
          price: JSON.stringify(fuelPrice),
        };
        console.warn('card', cardData);
        dispatch(AuthAction.MyFuelCards([...fuelCards, cardData]));
        try {
          await AsyncStorage.setItem(
            '@userMaxAllowance',
            JSON.stringify(newAllowance),
          );
        } catch (e) {
          console.warn(e);
        }
        NavigationService.navigate('Home');
      }
    } else if (selected === 'Battery Charge') {
      const fuelPrice = fuelStore[2].pricePerLiter * litres;
      const newAllowance = allowance - fuelPrice;
      if (fuelPrice > allowance) {
        Alert.alert('Sorry!', "You don't have enough fuel allowance");
      } else {
        console.warn(newAllowance);
        let cardData = {
          type: selected,
          fuelUsed: litres,
          price: JSON.stringify(fuelPrice),
        };
        console.warn('card', cardData);
        dispatch(AuthAction.MyFuelCards([...fuelCards, cardData]));
        try {
          await AsyncStorage.setItem(
            '@userMaxAllowance',
            JSON.stringify(newAllowance),
          );
        } catch (e) {
          console.warn(e);
        }
        NavigationService.navigate('Home');
      }
    }
  };

  return (
    <View style={CommonStyles.container}>
      <HeaderComp />
      <View
        style={{
          justifyContent: 'space-between' /* Aligning the elements in a row. */,
          flexDirection: 'row',
        }}>
        <View>
          <Text style={CommonStyles.textStyles.heading}>Add New </Text>
        </View>
        <View
          style={{
            flexDirection: 'row-reverse',
            alignSelf: 'flex-end',
            backgroundColor: '#F2F4F4',
            padding: 10,
            borderRadius: 15,
          }}>
          <Text
            style={{...CommonStyles.textStyles.buttonText, fontSize: 15}}
            onPress={() => getDeviceInfo()}>
            Show Device Info
          </Text>
        </View>
      </View>
      <View style={{marginVertical: Metrix.VerticalSize(50)}}>
        <View style={{flexDirection: 'column'}}>
          <Text
            style={{...CommonStyles.textStyles.buttonText, marginBottom: 10}}>
            Fuel Type:{' '}
          </Text>
          <SelectDropdown
            data={data}
            onSelect={(selectedItem, index) => {
              console.log(selectedItem, index);
              setSelected(selectedItem);
            }}
          />
        </View>

        <View
          style={{
            flexDirection: 'column',
            marginVertical: Metrix.VerticalSize(30),
          }}>
          <Text
            style={{...CommonStyles.textStyles.buttonText, marginBottom: 10}}>
            Litres/ Units:{' '}
          </Text>
          <View style={{height: Metrix.VerticalSize(50)}}>
            <TextInputComp
              value={litres}
              onChange={text => setLitres(text)}
              placeholder={'Enter Litres/ Charge Units Here'}
              secureWidth={true}
            />
          </View>
        </View>
      </View>
      <View
        style={{
          height: Metrix.VerticalSize(60),
          // marginVertical: Metrix.VerticalSize(20),
        }}>
        <Button
          color={Colors.black}
          onPress={() => onSubmit()}
          textColor={Colors.white}
          title={'Submit'}
        />
      </View>
    </View>
  );
};

export default CreateList;
