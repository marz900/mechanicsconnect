import { View, Text, Dimensions, TouchableOpacity, ScrollView } from 'react-native'
import React from 'react'

//dimensions
const {width} = Dimensions.get('screen');
const cardWith = width / 1.1;
const screen_width = Dimensions.get('window').width
const screen_height = Dimensions.get('window').height


//Ressources for
import {Avatar} from 'react-native-paper';
import {Colors} from '../config';
import Loader from '../components/Loader';


const GetPosition = () => {
  return (
    <View 
    style={{
      width: screen_width,
      height: screen_height,
      flexDirection: 'column',
      justifyContent: 'center',
    }}>
      <View 
      style={{
        flex: 1,
        flexDirection: 'column',
        alignItems: 'center',
        justifyContent: 'center',
      }}
      >
        <TouchableOpacity onPress={() => {}} style={{
          backgroundColor: Colors.blue,
          padding: 15,
          borderRadius: 10
        }}>
          <Text
          style={{
            fontSize: 15,
            fontWeight: 'bold',
            fontStyle: 'normal',
            color: Colors.white,
            textTransform: 'capitalize'
          }}
          >
            get location
          </Text>
        </TouchableOpacity>
        <View>
          <Text style={{
            fontWeight: 'bold',
            fontSize: 10,
            fontStyle: 'italic',
            color: Colors.black,
            textTransform: 'uppercase',
            fontFamily: 'Roboto'
          }}>
            -- or --
          </Text>
        </View>
        <TouchableOpacity onPress={() => {}} style={{
          backgroundColor: Colors.blue,
          padding: 15,
          borderRadius: 10
        }}>
          <Text
          style={{
            fontSize: 15,
            fontWeight: 'bold',
            fontStyle: 'normal',
            color: Colors.white,
            textTransform: 'capitalize',
            fontFamily: 'Roboto'
          }}
          >
            insert your location
          </Text>
        </TouchableOpacity>
      </View>
    </View>
  )
}

export default GetPosition;