/* eslint-disable react-native/no-inline-styles */
// import ImagePicker from 'react-native-image-crop-picker';
import React, {useState, useEffect, useContext} from 'react';
import {
  View,
  StyleSheet,
  TouchableOpacity,
  FlatList,
  Image,
  StatusBar,
  Text,
  Dimensions,
  RefreshControl,
} from 'react-native';

//Ressources for
import {Avatar} from 'react-native-paper';
import {Colors} from '../config';
import Loader from '../components/Loader';

//dimensions
const {width} = Dimensions.get('screen');
const cardWith = width / 1.1;
const screen_width = Dimensions.get('window').width
const screen_height = Dimensions.get('window').height

//External modules for...
import HomeCards from '../components/HomeCards';
import CustomRatingBar from '../rating/CustomeRating';
import ModalAll from '../modal/ModaleAll';
import ModalTester from '../modal/Modale';

//Icon Class...
import Animated from 'react-native-reanimated';
import ActionButton from 'react-native-action-button';
import Icon from 'react-native-vector-icons/Ionicons';
import FontAwesome from 'react-native-vector-icons/FontAwesome';
import IconMaterialIcons from 'react-native-vector-icons/MaterialIcons';
import Ionicons from 'react-native-vector-icons/Ionicons';
import Entypo from 'react-native-vector-icons/Entypo';
import MaterialCommunityIcons from 'react-native-vector-icons/MaterialCommunityIcons';

// Image picker dependancies
import ImagePicker from 'react-native-image-crop-picker';

//firebase dependencies
import firestore from '@react-native-firebase/firestore';

//import context
import {AppContext} from '../context/AppContext';

//temporary data
import productData from '../data/Data';

//Main function for...
const HomeScreen = ({navigation, route}) => {
  const [loading, setLoading] = useState(false);
  const [data, getData] = useState([]);
  const {user, userinfos, setUserInfos} = useContext(AppContext);
  const [visibleIndicator, setVisibleIndicator] = useState(true);
 
  const [onRefresh, setOnRefresh] = useState(false);

  useEffect(() => {
    getUserData();
    getMarker();
    console.debug('newwwwwwww----', userinfos);
  }, []);

  //Invoke_RefreshControl
  const Invoke_RefreshControl = () => {
    getData([]);
    getMarker();
 
  }

  //divider
  const divider = () => {
    return (
      <View
        style={{
          height: 1,
          width: "100%",
          backgroundColor: 'black',
        }}
      />
    );
  }

  const getUserData = async () => {
    try {
      const res = await firestore()
        .collection('Users')
        .doc(user.uid)
        .get()
        .then(documentSnapshot => {
          console.log('User exists: ', documentSnapshot.exists);
          if (documentSnapshot.exists) {
            setUserInfos(documentSnapshot.data());
            console.log('User data: ', documentSnapshot.data());
          }
        });
    } catch (error) {
      console.log(error);
    }
  };

  //get all data from service table
  // modif
  const getMarker = async () => {
    const userStatus = userinfos.status;
    if (userStatus != 'mechanic') {
        try {
          const list = [];
          await firestore()
            .collection('services')
            .get()
            .then(querySnapshot => {
              querySnapshot.forEach(doc => {
                const {
                  post,
                  postImg,
                  date,
                  description,
                  position,
                  price,
                  repair,
                } = doc.data();
                list.push({
                  id: doc.id,
                  ...doc.data(),
                });
              });
            });
          getData(list);
          setVisibleIndicator(false);
          console.log(list);
        } catch (error) {
          return error;
        }
    } else{
        try {
          const list = [];
          await firestore()
          .collection("services")
          .where("uid", "==", user.uid)
            .get()
            .then(querySnapshot => {
              querySnapshot.forEach(doc => {
                const {
                  post,
                  postImg,
                  date,
                  description,
                  position,
                  price,
                  repair,
                } = doc.data();
                list.push({
                  id: doc.id,
                  ...doc.data(),
                });
              });
            });
          getData(list);
          console.log(list)
        } catch (error) {
          return error;
        }
    }
  };

  let userStatus = userinfos.status;
  console.log('--------', userStatus)

  const renderItem = ({item, index}) => (
    <>
      <View>
        <TouchableOpacity
          onPress={() =>
            userStatus != 'customer'
              ? navigation.navigate('OfferScreen', {item})
              : navigation.navigate('OfferScreen', {item})
          }>
          <Image
            style={{
              height: 120,
              width: 150,
              marginHorizontal: 5,
              marginBottom: 10,
              borderRadius: 10,
            }}
            source={{uri: item.postImg}}
          />
        </TouchableOpacity>
      </View>
    </>
  );

  const SendData = () => {
    return (
      <ActionButton
        buttonColor={Colors.yellow}
        onPress={() => navigation.navigate('AddScreen')}
      />
    );
  };

  if (userStatus != 'mechanic') {
    return (
      <View
        style={{
          width: screen_width,
          height: screen_height,
          flexDirection: 'column',
        }}>
        <StatusBar
          barStyle="default"
          translucent
          backgroundColor="rgba(0,0,0,0)"
        />
        <Loader visible={loading} />
          <View
          style={{
            flexDirection: 'row',
            padding: '2%',
            paddingVertical: '3%',
            justifyContent: 'space-between',
            backgroundColor: Colors.black,
            flex: .8
          }}>
          <View
            style={{
              flexDirection: 'row',
            }}>
            <View>
              <Avatar.Image
                source={{
                  uri: userinfos.photoURL,
                }}
                size={60}
              />
            </View>
            <View
              style={{
                flexDirection: 'column',
                paddingHorizontal: '5%',
                justifyContent: 'space-evenly',
              }}>
              <Text
                style={[
                  styles.title,
                  {
                    color: Colors.yellow,
                  },
                ]}>
                {userinfos.displayName}
              </Text>
              <Text>
                <CustomRatingBar />
              </Text>
              <View style={{top: 5}}>
                <Text
                  style={{
                    fontSize: 10,
                    color: Colors.yellow,
                    textTransform: 'capitalize',
                  }}>
                  status: {''}
                  <Text style={{color: Colors.white, fontSize: 10}}>
                    {userinfos.status}
                  </Text>
                </Text>
              </View>
            </View>
          </View>
          <View style={{}}>
            <TouchableOpacity onPress={() => navigation.navigate('GetPosition')}>
              <FontAwesome
                name="map-marker"
                style={{
                  fontSize: 30,
                  color: Colors.yellow,
                  padding: 12,
                  borderRadius: 10,
                  borderColor: Colors.white,
                }}
              />
            </TouchableOpacity>
          </View>
        </View>

        <View style={{
          flex: .5,
          flexDirection: 'row',
          borderBottomWidth: 1,
          justifyContent: 'space-around',
          alignItems: 'center'
        }}>
          <View>
            <Text
              style={{
                fontSize: 16,
                color: Colors.black,
                textTransform: 'capitalize',
              }}>
              all
            </Text>
          </View>
          <View style={{borderRightColor: Colors.black, borderRightWidth: 1}} />
          <View
            style={{
              flexDirection: 'row',
              justifyContent: 'space-evenly',
              paddingHorizontal: 0.5,
              alignItems: 'center',
            }}>
            <FontAwesome name="map-marker" color={Colors.yellow} size={25} />
            <Text
              style={{
                fontSize: 16,
                color: Colors.black,
                textTransform: 'capitalize',
              }}
              onPress={() => navigation.push('GetPosition')}>
              {' '}
              set location
            </Text>
          </View>
            <View style={{borderRightColor: Colors.black, borderRightWidth: 1}} />
          <View
            style={{
              flexDirection: 'row',
              justifyContent: 'space-evenly',
              paddingHorizontal: 0.5,
              alignItems: 'center',
            }}>
            <IconMaterialIcons
              name="schedule"
              color={Colors.yellow}
              size={25}
            />
              <Text
                style={{
                  fontSize: 16,
                  color: Colors.black,
                  textTransform: 'capitalize',
                }}
                onPress={() => navigation.navigate('OrderScreen')}>
                {' '}
                schedules
              </Text>
          </View>
        </View>

        <View style={{
          flex: .3,
          padding: 8,

        }}>
          <Text
            style={{
              fontSize: 18,
              color: Colors.black,
              textTransform: 'capitalize',
              fontStyle: 'normal',
              fontWeight: 'bold',
            }}>
            buyer requests
          </Text>
        </View>
        
        <View style={{
          flex: 0.5,
          flexDirection: 'row',
          justifyContent: 'space-between',
          padding: 8,
        }}>
          <Text>
            <ModalTester />
          </Text>
          <Text>
            <ModalAll />
          </Text>
        </View>
        
        <View
          style={{
            flex: 4.5,
            alignItems: 'center',
          }}>
            <FlatList
              data={data}
              numColumns={2}
              keyExtractor={item => item.id.toString()}
              renderItem={renderItem}
              refreshControl={
                <RefreshControl
                  refreshing={onRefresh}
                  onRefresh={Invoke_RefreshControl}
                  tintColor="#C51162"
                  title='Loading Content...'
                  titleColor='blue'
              />
            }
          />
        </View>
      </View>
    );
  } else {
    return (
      <><View
        style={{
          width: screen_width,
          height: screen_height,
          flexDirection: 'column',
        }}>
        <StatusBar
          barStyle="light-content"
          translucent
          backgroundColor="rgba(0,0,0,0)" />
          <Loader visible={loading} />
        <View
          style={{
            flexDirection: 'row',
            padding: '2%',
            paddingVertical: '3%',
            justifyContent: 'space-between',
            backgroundColor: Colors.black,
            flex: .8
          }}>
          <View
            style={{
              flexDirection: 'row',
            }}>
            <View>
              <Avatar.Image
                source={{
                  uri: userinfos.photoURL,
                }}
                size={60} />
            </View>
            <View
              style={{
                flexDirection: 'column',
                paddingHorizontal: '5%',
                justifyContent: 'space-evenly',
              }}>
              <Text
                style={[
                  styles.title,
                  {
                    color: Colors.yellow,
                  },
                ]}>
                {userinfos.displayName}
              </Text>
              <Text>
                <CustomRatingBar />
              </Text>
              <View style={{ top: 5 }}>
                <Text
                  style={{
                    fontSize: 10,
                    color: Colors.yellow,
                    textTransform: 'capitalize',
                  }}>
                  status: {''}
                  <Text style={{ color: Colors.white, fontSize: 10 }}>
                    {userinfos.status}
                  </Text>
                </Text>
              </View>
            </View>
          </View>
          <View style={{}}>
            <TouchableOpacity onPress={() => navigation.navigate('GetPosition')}>
              <FontAwesome
                name="map-marker"
                style={{
                  fontSize: 30,
                  color: Colors.yellow,
                  padding: 12,
                  borderRadius: 10,
                  borderColor: Colors.white,
                }} />
            </TouchableOpacity>
          </View>
        </View>

        <View style={{
          flex: .5,
          flexDirection: 'row',
          borderBottomWidth: 1,
          justifyContent: 'space-around',
          alignItems: 'center'
        }}>
          <View>
            <Text
              style={{
                fontSize: 16,
                color: Colors.black,
                textTransform: 'capitalize',
              }}>
              all
            </Text>
          </View>
          <View style={{ borderRightColor: '#000', borderRightWidth: 1 }} />
          <View
            style={{
              flexDirection: 'row',
              justifyContent: 'space-evenly',
              paddingHorizontal: 0.5,
              alignItems: 'center',
            }}>
            <FontAwesome name="map-marker" color={Colors.yellow} size={25} />
            <Text
              style={{
                fontSize: 16,
                color: Colors.black,
                textTransform: 'capitalize',
              }}
              onPress={() => navigation.push('GetPosition')}>
              {' '}
              set location
            </Text>
          </View>
          <View style={{ borderRightColor: '#000', borderRightWidth: 1 }} />

          <View
            style={{
              flexDirection: 'row',
              justifyContent: 'space-evenly',
              paddingHorizontal: 0.5,
              alignItems: 'center',
            }}>
            <IconMaterialIcons
              name="schedule"
              color={Colors.yellow}
              size={25} />
            <Text
              style={{
                fontSize: 16,
                color: Colors.black,
                textTransform: 'capitalize',
              }}
              onPress={() => navigation.navigate('Orders')}>
              {' '}
              schedules
            </Text>
          </View>
        </View>

        <View style={{
          flex: .3,
          padding: 8,
        }}>
          <Text
            style={{
              fontSize: 18,
              color: Colors.black,
              textTransform: 'capitalize',
              fontStyle: 'normal',
              fontWeight: 'bold',
            }}>
            buyer requests
          </Text>
        </View>

        <View style={{
          flex: 0.5,
          flexDirection: 'row',
          justifyContent: 'space-between',
          padding: 8,
        }}>
          <Text>
            <ModalTester />
          </Text>
          <Text>
            <ModalAll />
          </Text>
        </View>

        <View
          style={{
            flex: 4.5,
            alignItems: 'center',
          }}>
          <FlatList
            data={data}
            numColumns={2}
            showsVerticalScrollIndicator={false}
            keyExtractor={item => item.id.toString()}
            renderItem={renderItem} 
            refreshControl={
              <RefreshControl
                refreshing={onRefresh}
                onRefresh={Invoke_RefreshControl}
                tintColor="#C51162"
                title='Loading Content...'
                titleColor='blue'
            />
          }
          />
            
        </View>
      </View><SendData /></>
    );
  }
};

export default HomeScreen;

const styles = StyleSheet.create({
  container: {
    flex: 1,
  },
  userInfoSection: {
    paddingHorizontal: '8%',
    marginBottom: 10,
    backgroundColor: Colors.black,
  },
  title: {
    fontSize: 14,
    fontWeight: 'bold',
    textTransform: 'capitalize',
  },
  caption: {
    fontSize: 14,
    lineHeight: 14,
    fontWeight: '500',
  },
  row: {
    flexDirection: 'row',
    marginBottom: 10,
  },
  infoBoxWrapper: {
    borderBottomColor: '#dddddd',
    borderBottomWidth: 1,
    borderTopColor: '#dddddd',
    borderTopWidth: 1,
    flexDirection: 'row',
    height: 100,
  },
  infoBox: {
    width: '50%',
    alignItems: 'center',
    justifyContent: 'center',
  },
  menuWrapper: {
    marginTop: 10,
  },
  menuItem: {
    flexDirection: 'row',
    paddingVertical: 15,
    paddingHorizontal: 30,
  },
  menuItemText: {
    color: '#777777',
    marginLeft: 20,
    fontWeight: '600',
    fontSize: 16,
    lineHeight: 26,
  },
  Uniform: {
    fontSize: 15,
    fontWeight: 'bold',
    fontStyle: 'normal',
    color: Colors.black,
  },
  Uniform2: {
    fontSize: 25,
    fontWeight: 'bold',
    fontStyle: 'normal',
  },
  Uniform1: {
    fontSize: 15,
    fontWeight: 'bold',
    fontStyle: 'normal',
    paddingVertical: '3%',
  },
  centeredView: {
    flex: 1,
    justifyContent: 'center',
    alignItems: 'center',
    marginTop: 5,
  },
  modalView: {
    margin: 20,
    backgroundColor: 'white',
    borderRadius: 8,
    padding: 45,
    height: 230,
    width: 180,
    alignItems: 'center',
    shadowColor: '#000',
    shadowOpacity: 0.25,
    shadowRadius: 4,
    justifyContent: 'space-evenly',
  },
  button: {
    borderRadius: 20,
    padding: 5,
  },
  buttonOpen: {
    backgroundColor: '#F194FF',
  },
  // buttonClose: {
  //   backgroundColor: '#2196F3',
  // },
  textStyle: {
    color: Colors.black,
    fontWeight: 'bold',
    textAlign: 'center',
    fontSize: 13,
  },
  modalText: {
    marginBottom: 15,
    textAlign: 'center',
    color: 'red',
  },
  actionButtonIcon: {
    fontSize: 20,
    height: 22,
    color: 'white',
  },
  row: {
    flex: 1,
    justifyContent: "space-around"
}
});
