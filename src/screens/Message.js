/* eslint-disable react-native/no-inline-styles */
import React, {useState, useContext, useEffect} from 'react';
import {
  View,
  Text,
  TouchableOpacity,
  Image,
  SafeAreaView,
  ScrollView,
  TextInput,
  StyleSheet,
  FlatList,
  Dimensions,
  StatusBar,
} from 'react-native';
import {
  Container,
  Card,
  UserInfo,
  UserImgWrapper,
  UserImg,
  UserInfoText,
  UserName,
  PostTime,
  MessageText,
  TextSection,
} from '../styles/MessageStyles';

//Icon Definitions
import {Avatar, Title, Caption, TouchableRipple} from 'react-native-paper';
import Icon from 'react-native-vector-icons/MaterialCommunityIcons';
import FontAwesome from 'react-native-vector-icons/FontAwesome';
import Feather from 'react-native-vector-icons/Feather';
import MaterialIcons from 'react-native-vector-icons/MaterialIcons';
import SimpleLineIcons from 'react-native-vector-icons/SimpleLineIcons';
import Ionicons from 'react-native-vector-icons/Ionicons';

//import Component Colors
import {Colors} from '../config';
const {width} = Dimensions.get('screen');
const cardWith = width / 1.1;
import CustomRatingBar from '../rating/CustomeRating';


//Dimensions
const screen_width = Dimensions.get('window').width
const screen_height = Dimensions.get('window').height

//context config
import { AppContext } from '../context/AppContext';


const MessageScreen = ({navigation}) => {
  const [data, getData] = useState([]);
  const { user, userinfos, setUserInfos } = useContext(AppContext);
  const [users, setUsers] = useState(null);

  useEffect(() => {
    getUsers();
    console.log('----------', userinfos)
  }, []);

  useEffect(() => {
    console.log(user.uid);
    console.log('Post Added!', user);
  }, []);

  const getUsers = async () => {
    let userStatus = userinfos.status
    if (userStatus != 'mechanic') {
      try {
        const querySanp = await firestore().collection('CustomerOffer').where('', "!=", "customer").get();
        // console.log('first000', querySanp)
        const allusers = querySanp.docs.map(docSnap => docSnap.data());
        console.log('fqiwufiefhiq', allusers)
        setUsers(allusers);
      } catch (e) {
        console.log(e)
      }
    } else {
      const querySanp = await firestore().collection('CustomerOffer').where("status", "!=", "mechanic").get();
        // console.log('first000', querySanp)
        const allusers = querySanp.docs.map(docSnap => docSnap.data());
        console.log('fqiwufiefhiq', allusers)
        setUsers(allusers);
      }

    }

  return (
    <View style={{
      width: screen_width,
      height: screen_height,
      flexDirection: 'column',
    }}>
      <StatusBar
            barStyle="dark-content"
            translucent
            hidden={false}
            backgroundColor="rgba(0,0,0,0)"
          />
      <View style={{
        flex: 1,
        flexDirection: 'row',
        justifyContent: 'space-between',
        backgroundColor: Colors.yellow,
        alignItems: 'center',
        padding: 5
      }}>
        <Text style={{
          fontSize: 20,
          fontStyle: 'normal',
          fontWeight: 'bold',
          color: Colors.white,
          textTransform: 'capitalize',
        }}>
          inbox
        </Text>
        <Ionicons name='ellipsis-vertical-outline' color={Colors.white}size={20} />
      </View>
      <View style={{
        flex: 5
      }}>
        <Container>
        <FlatList
          data={users}
          keyExtractor={item => item.userId}
          renderItem={({item}) => (
            <Card
              onPress={() =>
                navigation.navigate('ChatScreen', {
                  name: item.fullname,
                  uid: item.userId,
                })
              }>
              <UserInfo>
                <UserImgWrapper>
                  <UserImg source={{uri: item.photoURL}} />
                </UserImgWrapper>
                <TextSection>
                  <UserInfoText>
                    <UserName>{item.fullname}</UserName>
                    <PostTime>{item.messageTime}</PostTime>
                  </UserInfoText>
                  <MessageText>{item.messageText}</MessageText>
                </TextSection>
              </UserInfo>
            </Card>
          )}
        />
      </Container>
      </View>
    </View>
  );
};

export default MessageScreen;

const styles = StyleSheet.create({
  Uniform: {
    fontSize: 20,
    fontWeight: 'bold',
    fontStyle: 'normal',
    textTransform: 'capitalize',
    borderColor: Colors.yellow,
    borderWidth: 1,
    width: '80%',
    textAlign: 'center',
    padding: '5%',
  },

  btn: {
    alignItems: 'center',
    flex: 1,
    bottom: '-2.5%',
  },
  Uniform1: {
    fontSize: 25,
    fontWeight: 'bold',
    fontStyle: 'normal',
    paddingVertical: '4%',
    color: Colors.lighGray,
  },
  container: {
    flex: 1,
  },
  userInfoSection: {
    backgroundColor: Colors.yellow,
  },
  title: {
    fontSize: 24,
    fontWeight: 'bold',
  },
  caption: {
    fontSize: 14,
    lineHeight: 14,
    fontWeight: '500',
  },
  row: {
    flexDirection: 'row',
    marginBottom: 10,
  },
  infoBoxWrapper: {
    borderBottomColor: '#dddddd',
    borderBottomWidth: 1,
    borderTopColor: '#dddddd',
    borderTopWidth: 1,
    flexDirection: 'row',
    height: 100,
  },
  infoBox: {
    width: '50%',
    alignItems: 'center',
    justifyContent: 'center',
  },
  menuWrapper: {
    marginTop: 10,
  },
  menuItem: {
    flexDirection: 'row',
    paddingVertical: 15,
    paddingHorizontal: 30,
  },
  menuItemText: {
    color: '#777777',
    marginLeft: 20,
    fontWeight: '600',
    fontSize: 16,
    lineHeight: 26,
  },
});
