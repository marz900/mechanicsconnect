import {
  View,
  Text,
  StyleSheet,
  SafeAreaView,
  ScrollView,
  TouchableOpacity,
  Image,
  TextInput,
  ImageBackground,
  StatusBar,
  Button,
  ToastAndroid,
  Dimensions,
} from 'react-native';
import React, {useContext, useEffect, useState} from 'react';

//import react paper from 'react-paper';
import {useTheme} from 'react-native-paper';
import {Avatar, Title, Caption, TouchableRipple} from 'react-native-paper';

//import Icon from 'react-native-icon';
import FontAwesome from 'react-native-vector-icons/FontAwesome';
import Icon from 'react-native-vector-icons/MaterialCommunityIcons';
import Ionicons from 'react-native-vector-icons/Ionicons';
import Entypo from 'react-native-vector-icons/Entypo';
import MaterialIcons from 'react-native-vector-icons/MaterialIcons';
import FontAwesome5 from 'react-native-vector-icons/FontAwesome5';
import MaterialCommunityIcons from 'react-native-vector-icons/MaterialCommunityIcons';


//get screen dimensions
const screen_width = Dimensions.get('window').width
const screen_height = Dimensions.get('window').height


//custom import
import CustomRatingBar from '../rating/CustomeRating';
import {Colors} from '../config';
import {AppContext} from '../context/AppContext';

//responsive elements
import {
  widthPercentageToDP as wp,
  heightPercentageToDP as hp,
} from 'react-native-responsive-screen';

//supplient ressources
import DatePicker from 'react-native-date-picker';
import DateTimePicker from '@react-native-community/datetimepicker';

//firebase dependencies
import {firebase} from '@react-native-firebase/auth';
import auth from '@react-native-firebase/auth';
import firestore from '@react-native-firebase/firestore';

//import moment
import moment from 'moment';

//custom properties
const UselessTextInput = (props) => {
  return (
    <TextInput
      {...props}
      editable
      maxLength={40}
    />
  );
}

const currency = 'USD';

const OfferSent = ({navigation, route}) => {
  const {user,userinfos} = useContext(AppContext);
  const [city, onChangeCity] = useState(null);
  const [number, onChangeNumber] = useState(null);
  const [custdescription, setCustDescription] = useState(null); //
  const [date, setDate] = useState(new Date(Date.UTC(2012, 11, 20)));
  const [open, setOpen] = useState(false);
  const [dobLabel, setDobLabel] = useState('Select Date');
  const [getid, setGetId] = useState('');

  useEffect(() => {
    verifyData();
    console.log('user.uid', getid);
    // console.log('userinfos.Name', userinfos);
    // console.log('route.params.item.userId', route.params.item.uid);
  }, [getid])
  
  let image = route.params.item.postImg;
  let description = route.params.item.description
  let repair = route.params.item.repair
  let userSellerId = route.params.item.uid
  let mechanicUserName = route.params.item.userName

  const verifyData = async () => {
    try{
      await firestore().collection('CustomerOffer').get().then((querySnapshot) => {
        querySnapshot.forEach(snapshot => {
            let data = snapshot.id; 
            setGetId(data)
            console.log('------DATAAAAAA-------', data);
        })
    })
    }catch(e){
      console.log(e)
    }
  }
  
  const handleCustAdd = async () => {  
    try {
      await firestore()
        .collection('CustomerOffer')
        .add({
          uid: auth().currentUser.uid,
          userName: userinfos.displayName,
          mechanicUserName: mechanicUserName,
          userSellerId: userSellerId, 
          postImg: image,
          price: number,
          description: custdescription,
          position: city,
          date: dobLabel,
          repair: repair,
          postTime: firebase.firestore.Timestamp.now(),
        })
        .then((res) => {
          setTimeout(() => {
            navigation.navigate('OfferSuccessSent', {getid})
          }, 500);
          ToastAndroid.show('Custom Post Added !', ToastAndroid.SHORT);
        })
        .catch(e => {
          console.log(e);
        });
    } catch (error) {
      console.log('first', error);
    }
  };

  return (
    <View
      style={{
        width: screen_width,
        height: screen_height,
        flexDirection: 'column',
      }}>
      <StatusBar
        barStyle="default"
        translucent={true}
        hidden={false}
        backgroundColor="rgba(0,0,0,0)"
      />
      <View stytle={{
        flex: 2,
      }}>
        <View
        style={{
          width: '100%',
          flexDirection: 'row',
          padding: '2%',
          paddingVertical: '3%',
          justifyContent: 'space-between',
          backgroundColor: Colors.black,
        }}>
        <View
          style={{
            flexDirection: 'row',
          }}>
          <View>
            <Avatar.Image
              source={{
                uri: userinfos.photoURL,
              }}
              size={60}
            />
          </View>
          <View
            style={{
              flexDirection: 'column',
              paddingHorizontal: '5%',
              justifyContent: 'space-evenly',
            }}>
            <Text
              style={[
                styles.title,
                {
                  color: Colors.yellow,
                },
              ]}>
              {userinfos.displayName}
            </Text>
            <Text>
              <CustomRatingBar />
            </Text>
            <View style={{top: 5}}>
              <Text
                style={{
                  fontSize: 10,
                  color: Colors.yellow,
                  textTransform: 'capitalize',
                }}>
                status: {''}
                <Text style={{color: Colors.white, fontSize: 10}}>
                  {userinfos.status}
                </Text>
              </Text>
            </View>
          </View>
        </View>
        <View style={{}}>
          <TouchableOpacity>
            <FontAwesome
              name="map-marker"
              style={{
                fontSize: 30,
                color: Colors.yellow,
                padding: 12,
                borderRadius: 10,
                borderColor: Colors.white,
              }}
            />
          </TouchableOpacity>
        </View>
      </View>
      </View>

      <View 
      style={{
        flex: 4.4,
        padding: 5,
      }}>
          <View
            style={{
              width: '98%',
              height: '30%',
              alignSelf: 'center',
              borderColor: Colors.yellow,
              borderWidth: 2,
              padding: 10,
              marginTop: 10,
              flexDirection: 'row',
              justifyContent: 'space-between',
              borderRadius: 10,
            }}>
            <View
              style={{
                width: '50%',
                borderColor: Colors.yellow,
                borderWidth: 1,
                borderRadius: 10,
                left: -5,
              }}>
              <Image
                source={{
                  uri: image
                }}
                style={{
                  width: '100%',
                  height: '100%',
                  borderRadius: 10,
                }}
              />
            </View>
            <View
              style={{
                width: '50%',
                right: -3,
              }}>
              <Text
                style={{
                  height: '100%',
                  borderRadius: 10,
                  backgroundColor: Colors.white,
                  borderColor: Colors.yellow,
                  borderWidth: 2,
                  textAlign: 'justify',
                }}
                numberOfLines={3}>
                {description}
              </Text>
            </View>
          </View>
  
          <View style={styles.textWrapper}>
            <View
              style={{
                justifyContent: 'space-between',
                flexDirection: 'column',
                padding: 10,
              }}>
              <View
                style={{
                  width: '100%',
                }}>
                <Text
                  style={{
                    fontSize: 15,
                    fontWeight: 'bold',
                    textTransform: 'capitalize',
                  }}>
                  offer description
                </Text>
                <View
                  style={{
                    width: '90%',
                    alignItems: 'center',
                    borderColor: Colors.yellow,
                    borderWidth: 2,
                    borderRadius: 15,
                  }}>
                  <UselessTextInput
                    multiline
                    numberOfLines={4}
                    onChangeText={custdescription => setCustDescription(custdescription)}
                    style={{padding: 5, textAlign: 'justify'}}
                  />
                </View>
              </View>
            
          <View style={styles.action}>
          <Ionicons name="md-pricetag" color={Colors.yellow} size={25} />
            <TextInput
              placeholder="100 USD"
              onChangeText={number => onChangeNumber(number + `${'\t'}` + currency)}
              placeholderTextColor="#666666"
              keyboardType="number-pad"
              autoCorrect={false}
              style={[
                styles.textInput,
                {
                  color: Colors.black
                },
              ]}
            />
          </View>
            
            <View style={styles.action}>
              <FontAwesome name="map-marker" color={Colors.yellow} size={25} />
            <TextInput
              placeholderTextColor="#666666"
              onChangeText={city => onChangeCity(city)}
              autoCorrect={false}
              style={[
                styles.textInput,
                {
                  color: Colors.black
                },
              ]}
            />
          </View>
          <View style={styles.action}>
          <FontAwesome name="calendar-o" color={Colors.yellow} size={20} />
            <TouchableOpacity onPress={() => setOpen(true)}>
              <Text style={{color: '#666', marginLeft: 5, marginTop: 5}}>{dobLabel}</Text>
              <DatePicker
                modal
                open={open}
                date={date}
                format="YYYY-MM-DD"
                placeholder="select date"
                mode={'date'}
                maximumDate={new Date('2026-01-01')}
                minimumDate={new Date('2022-01-01')}
                onConfirm={date => {
                  setOpen(false);
                  setDate(date);
                  setDobLabel(date.toLocaleDateString());
                }}
                onCancel={() => {
                  setOpen(false);
                }}
              />
            </TouchableOpacity>

          </View>
            </View>
          </View>
      </View>
      <View 
      style={{ 
        flex: 1.8,
        alignItems: 'center',
        bottom: -30
        }}>
      <TouchableOpacity
              style={styles.commandButton}
              onPress={() => handleCustAdd()}>
              <Text style={styles.panelButtonTitle}>send offer</Text>
            </TouchableOpacity>
      </View>
    </View>
  );
};
export default OfferSent;

const styles = StyleSheet.create({
  container: {
    flex: 1,
  },
  userInfoSection: {
    marginBottom: 25,
  },
  title: {
    fontSize: 15,
    fontWeight: 'bold',
  },
  caption: {
    fontSize: 14,
    lineHeight: 14,
    fontWeight: '500',
  },
  row: {
    flexDirection: 'row',
    marginBottom: 10,
  },
  infoBoxWrapper: {
    flexDirection: 'row',
    height: '10%',
    marginVertical: '-2%',
  },
  infoBoxWrapperDes: {
    height: '12%',
    marginVertical: '-2%',
  },
  infoBox: {
    width: '50%',
    alignItems: 'center',
    justifyContent: 'center',
    marginBottom: '4%',
  },
  menuWrapper: {
    marginTop: 10,
  },
  menuItem: {
    flexDirection: 'row',
    paddingVertical: 15,
    paddingHorizontal: 30,
  },
  menuItemText: {
    color: '#777777',
    marginLeft: 20,
    fontWeight: '600',
    fontSize: 16,
    lineHeight: 26,
  },
  Uniform: {
    fontSize: 15,
    fontWeight: 'bold',
    fontStyle: 'normal',
    paddingVertical: '1%',
  },
  Uniform1: {
    fontSize: 15,
    fontWeight: 'bold',
    fontStyle: 'normal',
    paddingVertical: '3%',
  },
  commandButton: {
    padding: 10,
    borderRadius: 20,
    backgroundColor: Colors.yellow,
    width: '50%',
    justifyContent: 'center',
  },
  panel: {
    padding: 20,
    backgroundColor: '#FFFFFF',
    paddingTop: 20,
  },
  header: {
    backgroundColor: '#FFFFFF',
    shadowColor: '#333333',
    shadowOffset: {width: -1, height: -3},
    shadowRadius: 2,
    shadowOpacity: 0.4,
    paddingTop: 20,
    borderTopLeftRadius: 20,
    borderTopRightRadius: 20,
  },
  panelHeader: {
    alignItems: 'center',
  },
  panelHandle: {
    width: 40,
    height: 8,
    borderRadius: 4,
    backgroundColor: '#00000040',
    marginBottom: 10,
  },
  panelTitle: {
    fontSize: 27,
    height: 35,
  },
  panelSubtitle: {
    fontSize: 14,
    color: 'gray',
    height: 30,
    marginBottom: 10,
  },
  panelButton: {
    padding: 13,
    borderRadius: 10,
    backgroundColor: '#FF6347',
    alignItems: 'center',
    marginVertical: 7,
  },
  panelButtonTitle: {
    fontSize: 17,
    fontWeight: 'bold',
    color: 'white',
    textTransform: 'capitalize',
    textAlign: 'center',
  },
  action: {
    flexDirection: 'row',
    marginTop: 10,
    marginBottom: 7,
    borderBottomWidth: 2,
    borderBottomColor: Colors.yellow,
    paddingBottom: 1,
    borderRadius: 5
  },
  actionError: {
    flexDirection: 'row',
    marginTop: 10,
    borderBottomWidth: 0.5,
    borderBottomColor: Colors.yellow,
    paddingBottom: 5,
  },
  textInput: {
    flex: 1,
    marginTop: Platform.OS === 'ios' ? 0 : -18,
    paddingLeft: 10,
    color: '#05375a',
  },
  textWrapper: {
    height: hp('70%'), // 70% of height device screen
    width: wp('100%'), // 80% of width device screen
  },
  input: {
    height: 40,
    margin: 12,
    borderBottomWidth: 1,
    padding: 10,
  },
  inputCustom: {
    height: 40,
    width: 93,
    borderWidth: 1,
  },
});
