/* eslint-disable react-native/no-inline-styles */
import React, {useState, useEffect} from 'react';
import {
  View,
  Text,
  StatusBar,
  ScrollView,
  TouchableOpacity,
  ImageBackground,
  StyleSheet,
  TextInput,
  Platform,
  Image,
  Dimensions
} from 'react-native';

//Ressources
import { Colors } from '../config';
import {useTheme} from 'react-native-paper';
import CustomRatingBar from '../rating/CustomeRating';

//Dimensions
const screen_width = Dimensions.get('window').width
const screen_height = Dimensions.get('window').height

//import icon 
import Entypo from 'react-native-vector-icons/Entypo';
import Ionicons from 'react-native-vector-icons/Ionicons';
import Feather from 'react-native-vector-icons/Feather';
import FontAwesome from 'react-native-vector-icons/FontAwesome';
import MaterialIcons from 'react-native-vector-icons/MaterialIcons';
import MaterialCommunityIcons from 'react-native-vector-icons/MaterialCommunityIcons';

//responsive elements
import {
  widthPercentageToDP as wp,
  heightPercentageToDP as hp,
} from 'react-native-responsive-screen';

//firebase
import firestore from '@react-native-firebase/firestore';
import firebase from '@react-native-firebase/app';

//react-native-modal
import Modal from "react-native-modal";

//custom properties
const UselessTextInput = (props) => {
  return (
    <TextInput
      {...props} 
      editable
      maxLength={40}
    />
  );
}



const OfferScreen = ({route, navigation}) => {
  const {getid} = route.params;
  const [data, setData] = useState([])
  const [isModalVisible, setModalVisible] = useState(false);

  useEffect(() => {
    getUserData();
    console.log('pppp', route)
  }, [data]);

  const getUserData = async () => {
    try {
        await firestore()
        .collection('CustomerOffer')
        .doc(getid)
        .get()
        .then(documentSnapshot => {
          console.log('User exists: ', documentSnapshot.exists);

          if (documentSnapshot.exists) {
            setData(documentSnapshot.data());
            console.log('User data: ', documentSnapshot.data());
          }
        });
    } catch (error) {
      console.log(error);
    }
  };

  let image = data.postImg;
  let description = data.description
  let repair = data.repair
  let name = data.mechanicUserName
  let date = data.date
  let position = data.position
  let price = data.price
  
  return (
    <View style={{
        flexDirection: 'column',
        width: screen_width,
        height: screen_height,
    }}>
      <StatusBar
        barStyle="dark-content"
        translucent
        backgroundColor="rgba(0,0,0,0)"
      />
      <View 
        style={{
          flex: 1,
        }}>
        <View>
        <Image
          source={{
            uri: image
          }}
          style={{
            width: '100%',
            height: '100%',
            borderRadius: 10,
            borderBottomLeftRadius: 10,
            borderBottomRightRadius: 10,
          }}
          />
        </View>
        <View
          style={{
            marginTop: 40,
            flexDirection: 'row',
            alignItems: 'center',
            marginHorizontal: 20,
            justifyContent: 'space-between',
          }}>
          <Entypo
            name="chevron-small-left"
            style={{
              fontSize: 30,
              color: '#1E284A',
              backgroundColor: Colors.white,
              borderColor: Colors.white,
              borderRadius: Platform.OS === 'ios' ? 18 : 20,
              overflow: 'hidden',
              position: 'absolute',
              top: -125
            }}
            onPress={() => navigation.push('Home')}
          />
        </View>
      </View>

      <View style={{
        flex: 1,
        flexDirection: 'row',
        justifyContent: 'space-between',
        width: '92%',
        alignSelf: 'center'
      }}>
        <View style={{
          flexDirection: 'column',
          justifyContent: 'space-evenly',
        }}>
          <View style={{
          flexDirection: 'column',
        }}>
          <Text 
          style={{
            color: Colors.black,
            fontSize: 12,
            fontWeight: 'bold',
            fontStyle: 'normal',
            textAlign: 'center'
          }}>
            {name}
          </Text>
          <CustomRatingBar />
        </View>

        <View style={{
          flexDirection: 'row',
          alignItems: 'center',
        }}>
          <Text 
          style={{
            color: Colors.black,
            fontSize: 12,
            fontWeight: 'bold',
            fontStyle: 'normal',
            textAlign: 'center'
          }}>
            description 
          </Text>
        </View>
        </View>

        <View style={{
          flexDirection: 'column',
          justifyContent: 'space-evenly',
        }}>
          <View style={{
          flexDirection: 'row',
          alignItems: 'center',
        }}>
          <Ionicons name='ios-time-outline' color={Colors.yellow} size={20} />
          <Text 
          style={{
            color: Colors.black,
            fontSize: 12,
            fontWeight: 'bold',
            fontStyle: 'normal',
            textAlign: 'center'
          }}>
            {' '}4 p.m
          </Text>
        </View>

        <View style={{
          flexDirection: 'row',
          alignItems: 'center',
        }}>
          <Entypo name='tools' color={Colors.yellow} size={20} />
          <Text 
          style={{
            color: Colors.black,
            fontSize: 12,
            fontWeight: 'bold',
            fontStyle: 'normal',
            textAlign: 'center'
          }}>
            {' '}{repair}
          </Text>
        </View>
        </View>
        
      </View>

      <View 
      style={{
        flex: .9,
        borderColor: Colors.yellow,
        borderWidth: 2,
        borderRadius: 10,
        width: '92%',
        alignSelf: 'center',
      }}>
        <UselessTextInput
        multiline
        numberOfLines={4}
        value={description}
        disable={true}
        style={{padding: 5, textAlign: 'justify'}}
        />
      </View>
      <View style={{
        flex: 1.3,
        width: '95%',
        alignSelf: 'center',
        justifyContent: 'center',
      }}>
        <View style={styles.action}>
          <FontAwesome name="map-marker" color={Colors.yellow} size={25} />
          <Text
            style={[
              styles.textInput,
              {
                color: Colors.black,
              },
            ]}
          >
            {position}
          </Text>
        </View>
        <View style={styles.action}>
          <Entypo name="calendar" color={Colors.yellow} size={25} />
          <Text
            style={[
              styles.textInput,
              {
                color: Colors.black,

              },
            ]}
          >
            {date}
          </Text>
        </View>
        <View style={styles.action}>
          <Ionicons name="md-pricetag" color={Colors.yellow} size={25} />
          <Text
            style={[
              styles.textInput,
              {
               color: Colors.black,
              },
            ]}
          >
            {price}
          </Text>
        </View>
      </View>
      <View style={{
        flex: 1,
        justifyContent: 'center',
      }}>
        {
          isModalVisible != false ? 
          <View style={{
            flexDirection: 'column',

          }}>
            <Modal 
            isVisible={isModalVisible}
            onSwipeComplete={() => setModalVisible(false)}
            swipeDirection="down"
            swipeThreshold={200}
            >
                <TouchableOpacity
                    style={styles.commandButton}>
                    <Text style={styles.panelButtonTitle}>send offer</Text>
                </TouchableOpacity>
            </Modal>
          </View>
          : null
        }
      </View>
      
    </View>
  );
};

export default OfferScreen;

const styles = StyleSheet.create({
  text: {
    fontSize: 13,
    opacity: 0.5,
    fontWeight: '400',
    fontStyle: 'normal',
    color: '#1E284A',
    textTransform: 'capitalize',
  },
  action: {
    flexDirection: 'row',
    marginTop: 10,
    marginBottom: 10,
    borderBottomWidth: 1.5,
    borderBottomColor: Colors.yellow,
    borderRadius: 8
  },
  
  textInput: {
    flex: 1,
    paddingLeft: 10,
    color: Colors.yellow,
  },
  valuesItem: {
    color: '#1E284A',
    fontSize: 15,
    fontWeight: '700',
    textTransform: 'capitalize',
    lineHeight: 20,
  },
  btn: {
    height: 50,
    justifyContent: 'center',
    alignItems: 'center',
    marginTop: 20,
    backgroundColor: '#0A40A4',
    marginHorizontal: 20,
    borderRadius: 10,
  },
  btn1: {
    height: 50,
    justifyContent: 'center',
    alignItems: 'center',
    marginTop: 20,
    backgroundColor: Colors.white,
    marginHorizontal: 20,
    borderRadius: 10,
    flexDirection: 'row',
  },
  valuesText: {
    fontSize: 15,
    fontWeight: '700',
    textAlign: 'center',
    color: Colors.white,
    textTransform: 'capitalize',
    fontStyle: 'normal',
  },
  valuesText1: {
    fontSize: 15,
    fontWeight: '700',
    textAlign: 'center',
    color: '#1E284A',
    textTransform: 'capitalize',
    fontStyle: 'normal',
  },
  textWrapper: {
    height: hp('70%'), // 70% of height device screen
    width: wp('100%'), // 80% of width device screen
  },
  panelButtonTitle: {
    fontSize: 17,
    fontWeight: 'bold',
    color: 'white',
    textTransform: 'capitalize',
    textAlign: 'center',
  },
  commandButton: {
    padding: 10,
    borderRadius: 20,
    backgroundColor: Colors.yellow,
    alignSelf: 'center',
    opacity: 1
  },

});
