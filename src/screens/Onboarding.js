import { View, Text, StyleSheet, Image, TouchableOpacity, StatusBar } from 'react-native'
import React from 'react'
import Onboarding from 'react-native-onboarding-swiper';
import {Colors, Images, Metrix, NavigationService} from '../config';

const OnboardingScreen = ({ navigation, route }) => {

    React.useEffect(() => {
        const unsubscribe = navigation.addListener('');
    })
    
    
    const Skip = ({...props}) => (
    <TouchableOpacity
        style={{marginHorizontal:10}}
        {...props}
    >
        <Text style={{fontSize:16, fontWeight: 'bold', color: Colors.black}}>Prev</Text>
    </TouchableOpacity>
);

const Next = ({...props}) => (
    <TouchableOpacity
        style={{marginHorizontal:10}}
        {...props} 
    >
        <Text style={{fontSize:16, fontWeight: 'bold', color: Colors.black}}>Next</Text>
    </TouchableOpacity>
);

const Done = ({...props}) => (
    <TouchableOpacity
        style={{
            marginHorizontal: 10,
            backgroundColor: Colors.lighGray,
            borderColor: Colors.lighGray,
            borderWidth: 8,
            borderRadius: 15
        }}
        {...props}
    >
        <Text style={{ fontSize: 16, fontWeight: 'bold', color: Colors.black, padding: 2 }}>Get Start</Text>
    </TouchableOpacity>
);
    
    return (
        <Onboarding
        SkipButtonComponent={Skip}
        NextButtonComponent={Next}
        DoneButtonComponent={Done}
        onSkip={() => navigation.replace('ChoiceType')}
        onDone={() => navigation.navigate('ChoiceType')}
        titleStyles={styles.title}
        subTitleStyles={styles.subTitle}
        pages={[
            {
            backgroundColor: '#fff',
            image: <Image source={require('../assets/image/SSS1.png')} style={styles.image} />,
            title: 'Oh No !',
            subtitle: '',
            },
            {
            backgroundColor: '#fff',
            image: <Image source={require('../assets/image/keep.png')} style={styles.image} />,
            title: 'Snap it !',
            subtitle: '',
            },
            {
            backgroundColor: '#fff',
                image: <Image source={require('../assets/image/SSS2.png')} style={styles.image} />,
            title: 'Take it !',
            subtitle: '',
            },
            {
            backgroundColor: '#fff',
            image: <Image source={require('../assets/image/SSS3.png')} style={styles.image} />,
            title: 'WALLAH !',
            subtitle: '',
            },
        ]}
        />
        )
    }

export default OnboardingScreen;

const styles = StyleSheet.create({
    container: {
        flex: 1,
        alignItems: 'center',
        justifyContent: 'center',
    },
    image: {
        resizeMode: 'cover',
        width: '90%',
        height: '70%',
        borderRadius: 20
    },
    title: {
        color: 'blue',
        bottom: 155,
        fontSize: 20,
        fontWeight: 'bold',
        color: Colors.black,
    },
    subTitle: {
        fontSize: 15,
        bottom: 150,
        fontWeight: '800',
        fontStyle: 'normal',
        color: Colors.grey,
    }
})