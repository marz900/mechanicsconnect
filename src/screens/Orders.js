/* eslint-disable react-native/no-inline-styles */
import React, {useState, useEffect, useContext} from 'react';
import {
  View,
  Text,
  TouchableOpacity,
  Image,
  SafeAreaView,
  StyleSheet,
  StatusBar,
  FlatList,
  ToastAndroid,
  Dimensions,
  ScrollView,
  TextInput,
} from 'react-native';

//components...
import {Avatar, Card, Title, Caption, Paragraph} from 'react-native-paper';

//import Component Colors
import {Colors} from '../config';
import CustomRatingBar from '../rating/CustomeRating';


//Dimensions
const {width} = Dimensions.get('screen');
const cardWith = width / 1.1;
const screen_width = Dimensions.get('window').width
const screen_height = Dimensions.get('window').height

//Icon Class...
import Animated from 'react-native-reanimated';
import ActionButton from 'react-native-action-button';
import Icon from 'react-native-vector-icons/Ionicons';
import FontAwesome from 'react-native-vector-icons/FontAwesome';
import IconMaterialIcons from 'react-native-vector-icons/MaterialIcons';
import Ionicons from 'react-native-vector-icons/Ionicons';
import Entypo from 'react-native-vector-icons/Entypo';
import MaterialCommunityIcons from 'react-native-vector-icons/MaterialCommunityIcons';

//import context
import {AppContext} from '../context/AppContext';

//firebase dependencies
import firestore from '@react-native-firebase/firestore';

//custom properties
const UselessTextInput = (props) => {
  return (
    <TextInput
      {...props} 
      editable={false}
      maxLength={40}
    />
  );
}

export default function OrderScreen({navigation}) {
  const {user,userinfos} = useContext(AppContext);
  const [customdata, setCustomData] = useState([])
  


  let compare = userinfos.status;
  console.log('-----èèèè', compare)

  useEffect(() => {
    getItems();
    console.log('userinfos-------', customdata)
    console.log('user.uid', user.uid)
  }, []);

  const getItems = async () => {
      if(compare != 'mechanic'){
        try {
          const list = [];
          await firestore()
            .collection('CustomerOffer')
            .where("uid", "in", [user.uid])
            .get()
            .then(querySnapshot => {
              querySnapshot.forEach(doc => {
                const {
                  mechanicUserName,
                  postImg,
                  date,
                  description,
                  position,
                  price,
                  repair,
                  uid,
                  userName,
                  userSellerId
                } = doc.data();
                list.push({
                  id: doc.id,
                  ...doc.data(),
                });
              });
            });
            setCustomData(list);
        } catch (error) {
          return error;
        }
      }else{
        try {
          const list = [];
          await firestore()
            .collection('CustomerOffer')
            .where("userSellerId", "in", [user.uid])
            .get()
            .then(querySnapshot => {
              querySnapshot.forEach(doc => {
                const {
                  mechanicUserName,
                  postImg,
                  date,
                  description,
                  position,
                  price,
                  repair,
                  uid,
                  userName,
                  userSellerId
                } = doc.data();
                list.push({
                  id: doc.id,
                  ...doc.data(),
                });
              });
            });
            setCustomData(list);
        } catch (error) {
          return error;
        }
      }
      
    }

    if(compare != 'mechanic'){
      var renderItem = ({item, index}) => (
        <View style={{
          width: '100%',
          padding: 10,
        }}>
          <View style={{
            width: '100%',
            flexDirection: 'row',
            justifyContent: 'flex-start',
            alignItems: 'center',
            marginBottom: 2,
          }}>
            <Avatar.Image 
            // source={{uri: item}}
            size={50}
            />
            <Text 
            style={{
            fontSize: 15,
            fontWeight: 'bold',
            fontStyle: 'normal',
            paddingHorizontal: 2
    
            }}>
            {item.mechanicUserName}
            </Text>
          </View>
          <View style={{
          }}>
            <TouchableOpacity style={{
              height: 180,
              justifyContent: 'flex-start',
              flexDirection:'row',
              borderColor: Colors.yellow,
              borderWidth: 2,
              borderRadius: 15,
            }}
            onPress={() => navigation.navigate('TimelineScreen', {item})}
            >
              <View style={{
                flexDirection:'row',
                justifyContent: 'space-around',
              }}>
                <View style={{
                  flexDirection:'column',
                  justifyContent: 'space-evenly',
                }}>
                  <View style={{
                    flexDirection:'column',
                    justifyContent: 'space-evenly',
                    padding: 5,
                    }}>
                      <Image 
                      source={{uri: item.postImg}}
                      style={{
                        height: 80,
                        width: 100,
                        borderRadius: 15,
                      }}
                      />
                      <Text>
                        <CustomRatingBar />
                      </Text>
                    </View>
                  <View style={{
                    flexDirection: 'row',
                    alignItems: 'center',
                  }}>
                    <FontAwesome
                      name="map-marker"
                      style={{
                        fontSize: 15,
                        color: Colors.yellow,
                        padding: 12,
                        borderRadius: 10,
                        borderColor: Colors.white,
                      }}
                    />
                    <Text style={{
                      fontSize: 12,
                      fontWeight: "800",
                      fontStyle: "normal",
                      textAlign: 'auto',
                    }}>{item.position}</Text>
                  </View>
                </View>
                <View style={{
                  flexDirection: "column",
                  padding: 5
                }}>
                  <View style={{
                  }}>
                    <Text 
                    style={{
                      fontSize: 15,
                      fontWeight: 'bold',
                      fontStyle: 'normal',
                    }}>
                      description 
                    </Text>
                  </View>
                  <View 
                  style={{
                    borderColor: Colors.yellow,
                    borderWidth: 2,
                    borderRadius: 10,
                    width: 170,
                    height: 125,
                  }}>
                    <UselessTextInput
                    multiline
                    numberOfLines={4}
                    value={item.description}
                    style={{padding: 5, textAlign: 'justify'}}
                    />
                  </View>
                </View>
              </View>
              
            </TouchableOpacity>
          </View>
        </View>
      );
    }else{
      var renderItem = ({item, index}) => (
    
        <View style={{
          width: '100%',
          padding: 10,
        }}>
          <View style={{
            width: '100%',
            flexDirection: 'row',
            justifyContent: 'flex-start',
            alignItems: 'center',
            marginBottom: 2,
          }}>
            <Avatar.Image 
            // source={{uri: item}}
            size={50}
            />
            <Text 
            style={{
            fontSize: 15,
            fontWeight: 'bold',
            fontStyle: 'normal',
            paddingHorizontal: 2
    
            }}>
            {item.userName}
            </Text>
          </View>
          <View style={{
          }}>
            <TouchableOpacity style={{
              height: 180,
              justifyContent: 'flex-start',
              flexDirection:'row',
              borderColor: Colors.yellow,
              borderWidth: 2,
              borderRadius: 15,
            }}
            onPress={() => navigation.navigate('TimelineScreen', {item})}
            >
              <View style={{
                flexDirection:'row',
                justifyContent: 'space-around',
              }}>
                <View style={{
                  flexDirection:'column',
                  justifyContent: 'space-evenly',
                }}>
                  <View style={{
                    flexDirection:'column',
                    justifyContent: 'space-evenly',
                    padding: 5,
                    }}>
                      <Image 
                      source={{uri: item.postImg}}
                      style={{
                        height: 80,
                        width: 100,
                        borderRadius: 15,
                      }}
                      />
                      <Text>
                        <CustomRatingBar />
                      </Text>
                    </View>
                  <View style={{
                    flexDirection: 'row',
                    alignItems: 'center',
                  }}>
                    <FontAwesome
                      name="map-marker"
                      style={{
                        fontSize: 15,
                        color: Colors.yellow,
                        padding: 12,
                        borderRadius: 10,
                        borderColor: Colors.white,
                      }}
                    />
                    <Text style={{
                      fontSize: 12,
                      fontWeight: "800",
                      fontStyle: "normal",
                      textAlign: 'auto',
                    }}>{item.position}</Text>
                  </View>
                </View>
                <View style={{
                  flexDirection: "column",
                  padding: 5
                }}>
                  <View style={{
                  }}>
                    <Text 
                    style={{
                      fontSize: 15,
                      fontWeight: 'bold',
                      fontStyle: 'normal',
                    }}>
                      description 
                    </Text>
                  </View>
                  <View 
                  style={{
                    borderColor: Colors.yellow,
                    borderWidth: 2,
                    borderRadius: 10,
                    width: 170,
                    height: 125,
                  }}>
                    <UselessTextInput
                    multiline
                    numberOfLines={4}
                    value={item.description}
                    style={{padding: 5, textAlign: 'justify'}}
                    />
                  </View>
                </View>
              </View>
              
            </TouchableOpacity>
          </View>
        </View>
      );
    }
  

  return (
        <View style={{
          width: screen_width,
          height: screen_height,
          flexDirection: 'column',
          backgroundColor: '#F4F6F6'
        }}>
          <StatusBar
            barStyle="dark-content"
            translucent
            hidden={false}
            backgroundColor="rgba(0,0,0,0)"
          />
          <View style={{
            flex: 1,
            justifyContent: 'center',
            backgroundColor: Colors.yellow,
          }}>
            <Text 
            style={{
              textAlign: 'center',
              fontSize: 20,
              fontWeight: 'bold',
              fontStyle: 'normal',
              textTransform: 'capitalize',
              color: Colors.white
            }}>
              manager orders
            </Text>
          </View>
          <View style={{
            flex: 0.7,
            flexDirection: 'row',
            justifyContent: 'space-around',
            alignItems: 'center',
          }}>
            <View style={{
              borderColor: Colors.yellow,
              borderWidth: 1,
              borderRadius: 5,
              padding: 10,
              width: '46%'
            }}>
              <Text style={{
                fontSize: 15,
                fontStyle: 'normal',
                textTransform: 'capitalize',
                textAlign: 'center',
                color: Colors.black
              }}>
                schedules
              </Text>
            </View>
            <View style={{
              borderColor: Colors.yellow,
              borderWidth: 1,
              borderRadius: 5,
              padding: 10,
              width: '46%'
            }}>
              <Text style={{
                fontSize: 15,
                fontStyle: 'normal',
                textTransform: 'capitalize',
                textAlign: 'center',
                color: Colors.black
              }}
              onPress={() => navigation.navigate('OrdersDone')}
              >
                completed
              </Text>
            </View>
          </View>
          <View style={{
            flex: 6,
          }}>
            <View style={{
              marginBottom: '20%'
            }}>
              <FlatList
              data={customdata}
              renderItem={renderItem}
              keyExtractor={(item) => item.id}
              />
            </View>
          </View>
        </View>
  );
}

const styles = StyleSheet.create({
  container: {
    flex: 1,
  },
  userInfoSection: {
    paddingHorizontal: 30,
    marginBottom: 25,
    backgroundColor: Colors.yellow,
    padding: '5%',
  },
  title: {
    fontSize: 24,
    fontWeight: 'bold',
    color: Colors.white,
  },
  caption: {
    fontSize: 14,
    lineHeight: 14,
    fontWeight: '500',
  },
  infoBoxWrapper: {
    flexDirection: 'row',
    justifyContent: 'space-evenly',
    alignItems: 'center',
  },
  infoBox: {
    width: '50%',
    borderColor: Colors.yellow,
    borderWidth: 1,
    alignItems: 'center',
    padding: '3%',
    margin: '5%',
    flex: 1,
    bottom: '7%',
  },
  menuWrapper: {
    marginTop: 10,
  },
  menuItem: {
    flexDirection: 'row',
    paddingVertical: 15,
    paddingHorizontal: 30,
  },
  menuItemText: {
    color: '#777777',
    marginLeft: 20,
    fontWeight: '600',
    fontSize: 16,
    lineHeight: 26,
  },
  row: {
    flex: 1,
    flexDirection: 'column',
    marginVertical: 10,
    width: cardWith,
    height: 180,
    justifyContent: 'center',
    alignSelf: 'center',
    borderRadius: 15,
    color: 'red',
    borderColor: Colors.yellow,
  },
  textStyle: {
    textAlign: 'center',
    fontSize: 18,
    color: Colors.title,
    lineHeight: 36,
  },
  subStyle: {
    color: Colors.sub,
  },
  paragraphStyle: {
    textAlign: 'justify',
    color: Colors.para,
    fontSize: 12,
    opacity: 0.5,
    lineHeight: 16.7,
    flexWrap: 'wrap',
  },
  itemCustom: {
    fontSize: 10,
    fontWeight: 'bold',
    opacity: 0.6,
    color: '#1E284A',
    textTransform: 'capitalize',
  },
  textInput: {
    flex: 1,
    paddingLeft: 10,
    color: '#05375a',
  },

});
