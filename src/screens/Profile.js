import { View, Text, Image, ImageBackground, Linking, StyleSheet, Modal, TouchableOpacity, Alert, TextInput, SafeAreaView} from 'react-native';
import React, {useContext, useEffect, useState} from 'react'

//firebase dependances
import firestore from '@react-native-firebase/firestore';
import storage from '@react-native-firebase/storage';
import auth from '@react-native-firebase/auth';
import {GoogleSignin, statusCodes} from '@react-native-google-signin/google-signin';

//custom import
import CustomRatingBar from '../rating/CustomeRating';
import {Colors} from '../config';
import {AppContext} from '../context/AppContext';

//supplient ressources
import Animated from 'react-native-reanimated';
import BottomSheet from 'reanimated-bottom-sheet';
import ImagePicker from 'react-native-image-crop-picker';
import Button from '../components/Button';

//import react paper from 'react-paper';
import {useTheme} from 'react-native-paper';
import {Avatar, Title, Caption, TouchableRipple} from 'react-native-paper';
import {Images, Metrix, NavigationService} from '../config';

//import Icon from 'react-native-icon';
import FontAwesome from 'react-native-vector-icons/FontAwesome';
import Icon from 'react-native-vector-icons/MaterialCommunityIcons';
import Ionicons from 'react-native-vector-icons/Ionicons';
import MaterialIcons from 'react-native-vector-icons/MaterialIcons';
import FontAwesome5 from 'react-native-vector-icons/FontAwesome5';
import MaterialCommunityIcons from 'react-native-vector-icons/MaterialCommunityIcons';
import Feather from 'react-native-vector-icons/Feather';
import AntDesign from 'react-native-vector-icons/AntDesign';


const ProfileScreen = ({navigation, route}) => {
  const { user, userinfos, setUserInfos } = useContext(AppContext);

  useEffect(() => {
    console.log("oooo11", userinfos)
    getUser()
  }, []);

  let url = 'https://securitysystemservices.com/';

  const openUrl = async () => {
    const isSupported = await Linking.canOpenURL(url);
    if (isSupported) {
      await Linking.openURL(url)
    } else {
      Alert.alert(`Don't know how to open this url: ${url}`);
    }
  };


  const getUser = async () => {
    await firestore()
    .collection('Users')
    .doc(userinfos.uid)
    .get()
    .then((documentSnapshot) => {
      if( documentSnapshot.exists ) {
        console.log('User Data', documentSnapshot.data());
        setUserInfos(documentSnapshot.data());
      }
    })
  };

  

  let bs = React.createRef();
  let fall = new Animated.Value(1);

  const signOut = async () => {
    try {
      const out = await GoogleSignin.signOut();
      out !== undefined ? 
        navigation.replace('ChoiceType')
        :
        false
        return () => userReference();
    } catch (error) {
      console.error(error);
    }
  };
  

  return (
    <View style={{
      width: '100%',
      height: '100%',
      flexDirection: "column"
    }}>
      <View style={{
         alignItems: 'center',  
         borderBottomRightRadius: 25,
         borderBottomLeftRadius: 25,
         backgroundColor: Colors.yellow,
         flex: 1.7
      }}>
      <Animated.View
        useNativeDriver={true}
        style={{
          opacity: Animated.add(0.1, Animated.multiply(fall, 1.0)),
        }}>
          <TouchableOpacity>
            <View
              style={{
                borderRadius: 15,
                justifyContent: 'center',
                alignItems: 'center',
                bottom: -25
              }}>
              <View>
              <Avatar.Image
                source={{
                  uri: userinfos.photoURL,
                }}
                size={75}
              />
            </View>
            </View>
          </TouchableOpacity>
          <View style={{
            flexDirection: 'column',
            alignSelf: 'center'
          }}>
            <Text style={{marginTop: 26, fontSize: 18, fontWeight: 'bold', textAlign: 'center', color: Colors.white}}>
            {userinfos ? userinfos.displayName : ''}
            </Text>
            <CustomRatingBar />
          </View> 
      </Animated.View>
    </View>

    <View style={{
      flex: 3,
      bottom: Metrix.VerticalSize(-30),
      alignItems: 'center'
    }}>
        <View style={styles.action}>
          <FontAwesome name="user-o" color={Colors.yellow} size={20} />
          <Text style={styles.inputs} onPress={() => navigation.navigate('UpdateScreen')}>change name</Text>
          <MaterialCommunityIcons name='pencil-box-multiple' color={Colors.yellow} size={20}/>
        </View>
        <View style={styles.action}>
          <Feather name="key" color={Colors.yellow} size={20} />
          <Text style={styles.inputs} onPress={() => navigation.navigate('UpdatePass')}>change password</Text>
          <MaterialCommunityIcons name='pencil-box-multiple' color={Colors.yellow} size={20}/>
        </View>
        <View style={styles.action}>
          <AntDesign name="camerao" color={Colors.yellow} size={20} />
          <Text style={styles.inputs} onPress={() => navigation.navigate('UpdateImg')}>change image</Text>
          <MaterialCommunityIcons name='pencil-box-multiple' color={Colors.yellow} size={20}/>
        </View>
        <View style={styles.action1}>
          <AntDesign name="delete" color={Colors.yellow} size={20} />
          <Text style={{paddingHorizontal: 10, textTransform: 'capitalize'}} onPress={() => modalUpdate()}>delete my account</Text>
        </View>
    </View>

    <View style={{
      flex: 0.7,
      justifyContent: 'space-evenly',
      flexDirection: 'row',
      paddingBottom: 15
    }}>
          <TouchableOpacity onPress={signOut} style={styles.commandButton}>
          <Text style={styles.panelButtonTitle}>logout</Text>
          </TouchableOpacity>
    </View>
    <View style={{
      flex: 0.5,
    }}>
      <TouchableOpacity style={{
        width: '45%',
        borderRadius: 5,
        alignSelf: 'center'
      }}
      onPress={() => openUrl()}
      >
        <Text style={{
          fontSize: 10,
          fontWeight: 'bold',
          fontFamily:'Roboto',
          fontStyle: 'italic',
          color: 'tomato',
          paddingHorizontal: 10,
          textAlign: 'center',
          textTransform: 'uppercase'
        }}>
          security by sss
        </Text>
      </TouchableOpacity>
    </View>
    </View>
  )
}

export default ProfileScreen

const styles = StyleSheet.create({
  container: {
    flex: 1,
  },
  commandButton: {
    padding: 8,
    borderRadius: 15,
    backgroundColor: Colors.yellow,
    marginBottom: 30,
    width: '40%',
    alignSelf: 'center'
  },
  inputs: {
    textTransform: 'capitalize'
  },
  panel: {
    padding: 20,
    backgroundColor: '#FFFFFF',
    paddingTop: 20,
  },
  header: {
    backgroundColor: '#FFFFFF',
    shadowColor: '#333333',
    shadowOffset: {width: -1, height: -3},
    shadowRadius: 2,
    shadowOpacity: 0.4,
    paddingTop: 20,
    borderTopLeftRadius: 20,
    borderTopRightRadius: 20,
  },
  panelHeader: {
    alignItems: 'center',
  },
  panelHandle: {
    width: 40,
    height: 8,
    borderRadius: 4,
    backgroundColor: '#00000040',
    marginBottom: 10,
  },
  panelTitle: {
    fontSize: 27,
    height: 35,
  },
  panelSubtitle: {
    fontSize: 14,
    color: Colors.gray,
    height: 30,
    marginBottom: 10,
  },
  panelButton: {
    padding: 13,
    borderRadius: 10,
    backgroundColor: '#FF6347',
    alignItems: 'center',
    marginVertical: 7,
  },
  panelButtonTitle: {
    fontSize: 12,
    fontWeight: 'bold',
    color: 'white',
    textTransform: 'capitalize',
    textAlign: 'center',
  },
  action: {
    flexDirection: 'row',
    marginTop: 30,
    borderBottomWidth: 1,
    borderBottomColor: Colors.yellow,
    width: '90%',
    justifyContent: 'space-between',
  },
  action1: {
    flexDirection: 'row',
    marginTop: 30,
    borderBottomWidth: 1,
    borderBottomColor: Colors.yellow,
    width: '50%',
    justifyContent: 'space-evenly'
  },
  actionError: {
    flexDirection: 'row',
    marginTop: 10,
    borderBottomWidth: 1,
    borderBottomColor: '#FF0000',
    paddingBottom: 5,
  },
  textInput: {
    flex: 1,
    marginTop: Platform.OS === 'ios' ? 0 : -12,
    paddingLeft: 10,
    color: '#05375a',
  },
  textInput1: {
    flex: 1,
    marginTop: Platform.OS === 'ios' ? 0 : -12,
    paddingLeft: 10,
    color: '#05375a',
  },
  MainContainer: {
    flex: 1,
    padding: 10,
    alignItems: 'center',
    justifyContent: 'center',
    backgroundColor: '#FFF3E0'
  },

  modal: {
    width: 300,
    height: 200,
    backgroundColor: '#00B8D4',
    justifyContent: 'center',
    alignItems: 'center',
    borderRadius: 7,
    borderWidth: 1,
    borderColor: 'red'
  },

  text: {
    fontSize: 28,
    textAlign: 'center',
    color: 'white',
    padding: 10
  }
});

