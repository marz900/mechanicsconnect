/* eslint-disable react-native/no-inline-styles */
import React, {useState, useEffect, useContext} from 'react';
import {
  View,
  Text,
  TouchableOpacity,
  Image,
  SafeAreaView,
  StyleSheet,
  StatusBar,
  FlatList,
  ToastAndroid,
  Dimensions,
  ScrollView,
  TextInput,
} from 'react-native';

//components...
import {Avatar, Card, Title, Caption, Paragraph} from 'react-native-paper';

//import Component Colors
import {Colors} from '../config';
const {width} = Dimensions.get('screen');
const cardWith = width / 1.1;
import CustomRatingBar from '../rating/CustomeRating';


//Dimensions
const screen_width = Dimensions.get('window').width
const screen_height = Dimensions.get('window').height

//Icon Class...
import Animated from 'react-native-reanimated';
import ActionButton from 'react-native-action-button';
import Icon from 'react-native-vector-icons/Ionicons';

//icon vector
import AntDesign from 'react-native-vector-icons/AntDesign';
import FontAwesome from 'react-native-vector-icons/FontAwesome';
import IconMaterialIcons from 'react-native-vector-icons/MaterialIcons';
import Ionicons from 'react-native-vector-icons/Ionicons';
import Entypo from 'react-native-vector-icons/Entypo';
import MaterialCommunityIcons from 'react-native-vector-icons/MaterialCommunityIcons';

//import context
import {AppContext} from '../context/AppContext';


//custom properties
const UselessTextInput = (props) => {
    return (
      <TextInput
        {...props} 
        editable={false}
        maxLength={40}
      />
    );
  }

export default function TimelineScreen({navigation,route}) {
    const {item} = route.params;
  const {user,userinfos} = useContext(AppContext);

  useEffect(() => {
    console.log(item)
  }, []);

  let image = item.postImg;
  let date = item.date;
  let name = item.mechanicUserName;
  let description = item.description
  let price = item.price
  let locate = item.position
  
  return (
        <View style={{
          width: screen_width,
          height: screen_height,
          flexDirection: 'column',
          backgroundColor: '#F4F6F6'
        }}>
          <StatusBar
            barStyle="dark-content"
            translucent
            hidden={false}
            backgroundColor="rgba(0,0,0,0)"
          />
          <View style={{
            flex: 1,
            justifyContent: 'center',
            backgroundColor: Colors.yellow,
          }}>
        <View
        style={{
          flexDirection: 'row',
          padding: '2%',
          paddingVertical: '3%',
        }}>
          <View style={{
            justifyContent: 'center',
            paddingHorizontal: '5%',
          }}>
            <AntDesign name='arrowleft' color={Colors.white} size={30}
            onPress={() => navigation.push('OrderScreen')}
            />
          </View>
        <View
          style={{
            flexDirection: 'row',
          }}>
          <View>
            <Avatar.Image
              source={{
                uri: user.photoURL,
              }}
              size={50}
            />
          </View>
          <View
            style={{
              flexDirection: 'column',
              paddingHorizontal: '5%',
              justifyContent: 'space-evenly',
            }}>
            <Text
              style={[
                styles.title,
                {
                  color: Colors.white,
                },
              ]}>
              {name}
            </Text>
          </View>
        </View>
      </View>
          </View>
          <View style={{
            flex: 0.7,
            flexDirection: 'row',
            justifyContent: 'space-around',
            alignItems: 'center',
          }}>
            <View style={{
              borderColor: Colors.yellow,
              borderWidth: 1,
              borderRadius: 5,
              padding: 5,
              width: "46%",
            }}>
              <Text style={{
                fontSize: 15,
                fontStyle: 'normal',
                textTransform: 'capitalize',
                textAlign: 'center',
                color: Colors.black
              }}>
                timeline
              </Text>
            </View>
            <View style={{
              borderColor: Colors.yellow,
              borderWidth: 1,
              borderRadius: 5,
              padding: 5,
              width: "46%",
            }}>
              <Text style={{
                fontSize: 15,
                fontStyle: 'normal',
                textTransform: 'capitalize',
                textAlign: 'center',
                color: Colors.black
              }}>
                chat
              </Text>
            </View>
          </View>
          <View style={{
            flex: 6,
          }}>
            <View style={{
                flex: 1,
            }}>
                <Image
                source={{
                  uri: image
                }}
                style={{
                  width: '100%',
                  height: '100%',
                  borderBottomLeftRadius: 10,
                  borderBottomRightRadius: 10,
                }}
              />
            </View>
            <View style={{
                flex: 1.5,
                padding: 5,
                justifyContent: 'center',
            }}>
                <View style={{
                  paddingHorizontal: 16,
              }}>
                <Text 
                style={{
                  fontSize: 15,
                  fontWeight: 'bold',
                  fontStyle: 'normal'
                }}>
                  description 
                </Text>
              </View>
              <View 
              style={{
                borderColor: Colors.yellow,
                borderWidth: 2,
                borderRadius: 10,
                width: '90%',
                alignSelf: 'center',
              }}>
                <UselessTextInput
                multiline
                numberOfLines={4}
                value={description}
                style={{padding: 5, textAlign: 'justify'}}
                />
              </View>
            </View>
            <View style={{
                flex: 3.5,
                width: '95%',
                alignSelf: 'center'
            }}>
        <View style={styles.action}>
          <FontAwesome name="map-marker" color={Colors.yellow} size={25} />
          <Text
            style={[
              styles.textInput,
              {
                color: Colors.black,
              },
            ]}
          >
            {locate}
          </Text>
        </View>
        <View style={styles.action}>
        <Ionicons name='ios-time-outline' color={Colors.yellow} size={25} />
          <Text
            style={[
              styles.textInput,
              {
               color: Colors.black,
              },
            ]}
          >
            4 p.m
          </Text>
        </View>
        <View style={styles.action}>
          <Entypo name="calendar" color={Colors.yellow} size={25} />
          <Text
            style={[
              styles.textInput,
              {
                color: Colors.black,

              },
            ]}
          >
            {date}
          </Text>
        </View>
        <View style={styles.action}>
          <Ionicons name="md-pricetag" color={Colors.yellow} size={25} />
          <Text
            style={[
              styles.textInput,
              {
               color: Colors.black,
              },
            ]}
          >
            {price}
          </Text>
        </View>
        </View>
        </View>
        </View>
  );
}

const styles = StyleSheet.create({
  container: {
    flex: 1,
  },
  userInfoSection: {
    paddingHorizontal: 30,
    marginBottom: 25,
    backgroundColor: Colors.yellow,
    padding: '5%',
  },
  title: {
    fontSize: 20,
    fontWeight: 'bold',
    color: Colors.white,
  },
  caption: {
    fontSize: 14,
    lineHeight: 14,
    fontWeight: '500',
  },
  infoBoxWrapper: {
    flexDirection: 'row',
    justifyContent: 'space-evenly',
    alignItems: 'center',
  },
  infoBox: {
    width: '50%',
    borderColor: Colors.yellow,
    borderWidth: 1,
    alignItems: 'center',
    padding: '3%',
    margin: '5%',
    flex: 1,
    bottom: '7%',
  },
  menuWrapper: {
    marginTop: 10,
  },
  menuItem: {
    flexDirection: 'row',
    paddingVertical: 15,
    paddingHorizontal: 30,
  },
  menuItemText: {
    color: '#777777',
    marginLeft: 20,
    fontWeight: '600',
    fontSize: 16,
    lineHeight: 26,
  },
  row: {
    flex: 1,
    flexDirection: 'column',
    marginVertical: 10,
    width: cardWith,
    height: 180,
    justifyContent: 'center',
    alignSelf: 'center',
    borderRadius: 15,
    color: 'red',
    borderColor: Colors.yellow,
  },
  textStyle: {
    textAlign: 'center',
    fontSize: 18,
    color: Colors.title,
    lineHeight: 36,
  },
  subStyle: {
    color: Colors.sub,
  },
  paragraphStyle: {
    textAlign: 'justify',
    color: Colors.para,
    fontSize: 12,
    opacity: 0.5,
    lineHeight: 16.7,
    flexWrap: 'wrap',
  },
  itemCustom: {
    fontSize: 10,
    fontWeight: 'bold',
    opacity: 0.6,
    color: '#1E284A',
    textTransform: 'capitalize',
  },
  action: {
    flexDirection: 'row',
    marginTop: 10,
    marginBottom: 10,
    borderBottomWidth: 1.5,
    borderBottomColor: Colors.lighGray,
    borderRadius: 8
  },
  textInput: {
    flex: 1,
    paddingLeft: 10,
  },

});
